<?php

namespace Lexik\Bundle\CrudBundle\Tests\Form\DataTransformer;

use Lexik\Bundle\CrudBundle\Tests\TestCase;
use Lexik\Bundle\CrudBundle\Form\DataTransformer\DateRangeTransformer;
use Symfony\Component\Form\Exception\UnexpectedTypeException;

/**
 * Unit test for DateRangeTransformer class.
 *
 * @package LexikCrudBundle
 * @author  Bérenger VIDAL <b.vidal@lexik.fr>
 */
class DateRangeTransformerTest extends TestCase
{
    /**
     * @var DateRangeTransformer
     */
    protected $transformer;

    /**
     * @var array
     */
    protected $dateRanges;

    /**
     * @var array
     */
    protected $dateRangesString;

    protected function setUp()
    {
        parent::setUp();

        $this->transformer = new DateRangeTransformer();
        $this->dateRanges = array("from" => new \DateTime('2011/01/01'), "to" => new \DateTime('2011/12/31'));
        $this->dateRangesString = (substr(\Locale::getDefault(), 0, 5) === 'fr_FR')
            ? array("from" => "1 janv. 2011 00:00", "to" => "31 déc. 2011 00:00")
            : array("from" => "Jan 1, 2011 12:00 AM", "to" => "Dec 31, 2011 12:00 AM");
    }

    /**
     * @expectedException Symfony\Component\Form\Exception\UnexpectedTypeException
     */
    public function testTransformWrongType()
    {
        $this->transformer->transform('wrong');
    }

    /**
     * @expectedException Symfony\Component\Form\Exception\UnexpectedTypeException
     */
    public function testReverseTransformWrongType()
    {
        $this->transformer->reverseTransform('wrong');
    }

    public function testTransform()
    {
        $this->assertEquals(array(), $this->transformer->transform(array()));
        $this->assertEquals($this->dateRangesString, $this->transformer->transform($this->dateRanges));
    }

    public function testReverseTransform()
    {
        $this->assertEquals(array(), $this->transformer->reverseTransform(array()));
        $this->assertEquals($this->dateRanges, $this->transformer->reverseTransform($this->dateRangesString));
    }
}