<?php

namespace Lexik\Bundle\CrudBundle\Tests\Form\Type;

use Lexik\Bundle\CrudBundle\Tests\TestCase;
use Lexik\Bundle\CrudBundle\Form\Type\EntityChoicerType;
use Lexik\Bundle\CrudBundle\Tests\Fixtures\Article;
use Lexik\Bundle\CrudBundle\Form\Extension\CrudExtension;

use Symfony\Component\Form\ResolvedFormTypeFactory;
use Symfony\Component\Form\FormRegistry;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\Form\Extension\Core\CoreExtension;
use Symfony\Bridge\Doctrine\Form\DoctrineOrmExtension;
use Symfony\Bundle\DoctrineBundle\Registry;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * Unit test for EntityChoicerType form type.
 *
 * @package LexikCrudBundle
 * @author  Jeremy Barthe <j.barthe@lexik.fr>
 */
class EntityChoicerTypeTest extends TestCase
{
    protected $factory;

    protected $registry;

    /**
     * @var array
     */
    protected $entities;

    /**
     * @var ArrayCollection
     */
    protected $arrayCollection;

    protected function setUp()
    {
        parent::setUp();

        // init fixtures
        $this->entities['article1'] = new Article();
        $this->entities['article1']->setId(1);
        $this->entities['article1']->setTitle('Article 1');
        $this->em->persist($this->entities['article1']);

        $this->entities['article2'] = new Article();
        $this->entities['article2']->setId(2);
        $this->entities['article2']->setTitle('Article 2');
        $this->em->persist($this->entities['article2']);

        $this->em->flush();

        $this->arrayCollection = new ArrayCollection($this->entities);

        // init form factory
        $managerRegistery = $this->getMock('Symfony\Bridge\Doctrine\RegistryInterface');
        $managerRegistery->expects($this->any())
            ->method('getEntityManager')
            ->with($this->equalTo('default'))
            ->will($this->returnValue($this->em));

        $resolvedFormTypeFactory = new ResolvedFormTypeFactory();

        $this->registry = new FormRegistry(array(
            new CoreExtension(),
            new DoctrineOrmExtension($managerRegistery),
            new CrudExtension($managerRegistery),
        ), $resolvedFormTypeFactory);

        $this->factory = new FormFactory($this->registry, $resolvedFormTypeFactory);
    }

    protected function tearDown()
    {
        $this->factory         = null;
        $this->registry        = null;
        $this->entities        = null;
        $this->arrayCollection = null;
    }

    public function testSingleEntity()
    {
        // without entity
        $field = $this->factory->createNamed('article', 'lexik_entity_choicer', null, array(
            'em'       => 'default',
            'class'    => 'Lexik\\Bundle\\CrudBundle\\Tests\\Fixtures\\Article',
            'required' => false,
        ));

        $this->assertEquals(null, $field->createView()->get('entity'));

        // with entity
        $field->setData($this->entities['article1']);
        $this->assertEquals($this->entities['article1'], $field->createView()->get('entity'));
    }

    public function testMultipleEntity()
    {
        // without entity
        $field = $this->factory->createNamed( 'article', 'lexik_entity_choicer', null, array(
            'em'       => 'default',
            'class'    => 'Lexik\\Bundle\\CrudBundle\\Tests\\Fixtures\\Article',
            'multiple' => true,
            'required' => false,
        ));

        $this->assertEquals(array(), $field->createView()->get('entities'));

        // with entity
        $field->setData($this->arrayCollection);
        $this->assertEquals(
            array(
                1 => 'Article 1',
                2 => 'Article 2',
            ),
            $field->createView()->get('entities')
        );
    }
}
