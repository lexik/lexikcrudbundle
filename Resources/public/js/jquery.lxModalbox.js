/**
 * Modalbox tools.
 *
 * Depends:
 *     jQuery
 *     jQuery UI
 *     jQuery UI ThemeRoller
 *     jquery.form.js
 *
 * @author Jeremy Barthe <j.barthe@lexik.fr>
 */

(function($){
    $.fn.lxModalbox = function(options){

        var defaults = {
            dialogElementPrefix: 'lx-modalbox-dialog',
            dialogOptions: {
                autoOpen: false,
                width:    950,
                height:   500,
                modal:    true
            }
        };

        var dialogElement;

        var settings = $.extend(defaults, options);

        var listenToClickOnLinkElement = function(e, href, blockAjax) {
            e.preventDefault();

            // by default do not block ajax navigation
            blockAjax = blockAjax || false;

            // validate URL
            if ((typeof href === 'undefined') && (
                $.trim($(this).attr('href')) === ''
                || $(this).attr('href') === '#'
                || $(this).attr('href') === 'javascript:void(0)'
            )) {
                return;
            }

            var dialog = getUiDialog();

            // add loader
            dialog
                .empty()
                .addClass('loader');

            if ($(e.target).attr('data-dialog-content') != undefined) {
                var html = $(e.target).attr('data-dialog-content') || $(e.target).html();
                renderHtml(html, dialog, blockAjax);
            } else {
                $.ajax({
                    type: 'GET',
                    cache: false,
                    url: $(this).attr('href') || href,
                    success: function(html) {
                        renderHtml(html, dialog, blockAjax);
                    }
                });
            }
        }

        var listenToSubmitOnFormElement = function(e) {
            e.preventDefault();

            var form = $(this);
            var dialog = getUiDialog();

            // add loader
            dialog
                .empty()
                .addClass('loader');

            $(form).ajaxSubmit({
                type: form.attr('method'),
                url:  form.attr('action'),
                data: { _xml_http_request: true },
                success: function(html) {
                    if (jQuery.type(html) == 'object') {
                        dialog.dialog('close');

                        // custom event: form success
                        dialog.data('opener').trigger('dialog.form.success', [html]);

                        return;
                    }

                    renderHtml(html, dialog);
                }
            });
        }

        var renderHtml = function(html, dialog, blockAjax) {
            // cascading dialog box
            $(html).find('[data-dialog]').lxModalbox();

            dialog.html(html);

            if ( ! blockAjax) {
                dialog.find('[data-dialog-close]').bind('click', function(e) {
                    e.preventDefault();
                    dialog.dialog('close');
                });

                // click on dialog box
                dialog.find('a:not(.no-dialog-ajax):not([data-dialog]):not([data-dialog-close]):not(.lx-widget-modalbox-choice-select)').bind('click', listenToClickOnLinkElement);

                // submit form on dialog box
                dialog.find('form').bind('submit', listenToSubmitOnFormElement);
            }

            dialog.trigger('dialog.render_html', [dialog]);

            // remove loader
            dialog.removeClass('loader');

            if ( ! dialog.dialog('isOpen')) {
                dialog.dialog('open');
            }
        }

        var getUiDialog = function() {
            var dialog = $(dialogElement);
            if (dialog.length == 0) {
                var dialog = $('<div id="' + dialogElement.replace('#', '') + '" />').appendTo(document);
                dialog.data('opener', null);

                return dialog.dialog(settings.dialogOptions);
            }

            return dialog;
        }

        var getUniqueID = function() {
            var uniqueID = new Date();
            return uniqueID.getTime();
        }

        function init() {
            var self = $(this);

            dialogElement = '#' + settings.dialogElementPrefix + getUniqueID();

            self.bind('click', function(e) {
                e.preventDefault();

                var dialog = getUiDialog();

                self.trigger('dialog.init', [dialog]);

                dialog.dialog('option', 'title', self.attr('data-dialog-title') || self.attr('title'));

                if (self.attr('data-dialog-width')) {
                    dialog.dialog('option', 'width', self.attr('data-dialog-width'));
                }
                if (self.attr('data-dialog-height')) {
                    dialog.dialog('option', 'height', self.attr('data-dialog-height'));
                }

                dialog.bind('dialogclose', function() {
                    self.trigger('dialog.close', [dialog, self]);
                });

                dialog.data('opener', self); // store opener

                var blockAjax = self.attr('data-dialog-ajax') != undefined ? false : true;

                var event = jQuery.Event('click');
                event.target = self;
                var href = $(this).attr('data-href') || $(this).attr('href');
                listenToClickOnLinkElement(e, href, blockAjax);

                // custom event: dialog box's opened for the first time
                dialog.bind('dialogopen', function(event, ui) {
                    self.trigger('dialog.open', [dialog, self]);
                });

                return false;
            });
        }

        $(this).each(init);

        return $(this);
    };

    $(function(){
        $('[data-dialog]').lxModalbox();
    });
})(jQuery);
