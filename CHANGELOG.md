CHANGELOG
=========

* v1.2.0 (2013-XX-XX)

 * b59f16d8: [BC break] pre/postPersist and pre/postUpdate are replaced by persist and update

* v1.1.2 (2012-12-18)

* v1.1.1 (2012-12-14)

* v1.1.0 (2012-09-10)

* v1.0.0 (2012-07-17)
