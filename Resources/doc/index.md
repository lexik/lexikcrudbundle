Create a CRUD controller
========================


Initailize a new CRUD
---------------------

1. Create the controller class
2. Load the routing from the controller class
3. Define the controller as a service

I suppose we need a CRUD for the following entity:

```php
<?php

namespace Project\Bundle\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class User 
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $email;
}
```

##### 1. Create the controller class

Just create a controller class and make it entends from `Lexik\Bundle\CrudBundle\Controller\CrudController`.
Then add the `@Route` annotation to define the route prefix, the route name and also the service id of this crud.

```php
<?php

namespace Project\Bundle\UserBundle\Controller;

use Lexik\Bundle\CrudBundle\Controller\CrudController;
use Lexik\Bundle\CrudBundle\Routing\Route;

/**
 * @Route("/user", name="crud_user", serviceId="my_project.crud.user")
 */
class UserController extends CrudController
{
}
```

##### 2. Load the routing from the controller class

Then load CRUD routes from the controller:

```yaml
# routing.yml
crud_user:
    resource: "@UserBundle/Controller/UserController.php"
    type:     lexikcrud
```

*Don't forget `type: lexikcrud` in the definition of the route.*

##### 3. Define the controller as a service

Now define the controller class as a service with the `lexik_crud.controller` tag:

```xml
<service id="my_project.crud.user" class="Project\Bundle\UserBundle\Controller\UserController">
    <argument type="service" id="service_container" />
    <argument>Project\Bundle\UserBundle\Entity\User</argument>
    <tag name="lexik_crud.controller" alias="user_controller" />
</service>
``` 

After these 3 previous steps you should get a basic crud, you can display the list page. 
But you won't see any filter on the list and the edit/new page won't work.


Define form and filter form classes
-----------------------------------

Now let's define the form and form filter classes in the controller class properties:

```php
<?php

namespace Project\Bundle\UserBundle\Controller;

use Lexik\Bundle\CrudBundle\Controller\CrudController;
use Lexik\Bundle\CrudBundle\Routing\Route;

/**
 * @Route("/user", name="crud_user", serviceId="my_project.crud.user")
 */
class UserController extends CrudController
{
    // form filter type to use on liste page
    protected $filterClassName = 'Project\Bundle\UserBundle\Filter\UserType';

    // form type to use to create and edit a User.
    protected $formClassName = 'Project\Bundle\UserBundle\Form\UserType';
    
    // in case of you need to use a different form type for the edit page you can add this property
    protected $editFormClassName = 'Project\Bundle\UserBundle\Filter\UserEditType';
}
```

*Form filters work with `LexikFormFilterBundle` so the bundle have t be installed to be able to filter the list.*


Customize the list
------------------

##### Override default query builder

If you need to customize the Doctrine query builder used for the list query you can override the `getListQueryBuilder()` method:

```php
<?php

namespace Project\Bundle\UserBundle\Controller;

use Lexik\Bundle\CrudBundle\Controller\CrudController;
use Lexik\Bundle\CrudBundle\Routing\Route;

/**
 * @Route("/user", name="crud_user", serviceId="my_project.crud.user")
 */
class UserController extends CrudController
{
    /**
     * Returns a query builder to use to build the list query.
     *
     * @param string $alias
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function getListQueryBuilder($alias)
    {
        return $this->get('doctrine.orm.entity_manager')
            ->getRepository($this->getEntityName())
            ->createQueryBuilder($alias)
            ...
            // add whatever you need, joins, conditions, ...   
        ;
    }
}
```

##### Change list default sort and items per pages

```php
<?php

namespace Project\Bundle\UserBundle\Controller;

use Lexik\Bundle\CrudBundle\Controller\CrudController;
use Lexik\Bundle\CrudBundle\Routing\Route;

/**
 * @Route("/user", name="crud_user", serviceId="my_project.crud.user")
 */
class UserController extends CrudController
{
    // set this property if you need to change the default way to sort the list.
    protected $listDefaultSort = array('id', 'asc');

    // change number of items per pages on the list
    protected $maxPerPageChoices = array(10, 25, 50);
}
```

##### Batch actions

TODO :p


Templates
---------

##### Template selector

The CrudController class use a template selector object to automatically guess the template to load.

TODO :p

##### Extra parameters

If you need to pass to additionnal variables to the templates youn can override the following methods form the CrudController class.

```php
<?php

namespace Project\Bundle\UserBundle\Controller;

use Lexik\Bundle\CrudBundle\Controller\CrudController;
use Lexik\Bundle\CrudBundle\Routing\Route;

/**
 * @Route("/user", name="crud_user", serviceId="my_project.crud.user")
 */
class UserController extends CrudController
{
    protected function getListExtraParameters()
    {
        return array( ... );
    }
    
    protected function getShowExtraParameters($object)
    {
        return array( ... );
    }
    
    protected function getNewExtraParameters($object)
    {
        return array( ... );
    }
    
    protected function getEditExtraParameters($object)
    {
        return array( ... );
    }
}
```


Routing
-------

##### Route Generator

TODO :p

##### Add parameters in routes pattern

TODO :p


Security and roles
-------------------

##### Crud Security Handler

TODO :p

##### Roles per actions

TODO :p


pre/post Persist and pre/post Update methods
--------------------------------------------

TODO :p



