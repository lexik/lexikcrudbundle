<?php

namespace Lexik\Bundle\CrudBundle\Security\Handler;

use Lexik\Bundle\CrudBundle\Security\Handler\SecurityHandlerInterface;

use Symfony\Component\Security\Core\SecurityContextInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationCredentialsNotFoundException;

/**
 * A security handler for CRUD controller based on roles authorization.
 *
 * @package LexikCrudBundle
 * @author  Jeremy Barthe <j.barthe@lexik.fr>
 */
class RoleSecurityHandler implements SecurityHandlerInterface
{
    /**
     * @var SecurityContextInterface
     */
    protected $securityContext;

    /**
     * @var string
     */
    protected $baseRoleName;

    /**
     * Define an array of expected roles for each actions.
     *
     * @var array
     */
    protected $rolesPerActions = array(
        'LIST'   => array('LIST'),
        'RESET'  => array('LIST'),
        'NEW'    => array('NEW'),
        'EDIT'   => array('EDIT'),
        'SHOW'   => array('SHOW'),
        'BATCH'  => array('BATCH'),
        'DELETE' => array('DELETE'),
    );

    /**
     * Constructor.
     *
     * @param SecurityContextInterface $securityContext
     */
    public function __construct(SecurityContextInterface $securityContext)
    {
        $this->securityContext = $securityContext;
    }

    /**
     * {@inheritDoc}
     */
    public function setBaseRoleName($baseRoleName)
    {
        $this->baseRoleName = strtoupper($baseRoleName);
    }

    /**
     * {@inheritDoc}
     */
    public function mergeRolesPerActions(array $rolesPerActions)
    {
        foreach ($this->rolesPerActions as $action => $roles) {
            if (isset($rolesPerActions[$action])) {
                $this->rolesPerActions[$action] = $rolesPerActions[$action];
            } else {
                $this->rolesPerActions[$action] = array();
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    public function isGranted($attributes, $object = null)
    {
        $attributes = $this->buildRolesAttributes($attributes);

        try {
            return $this->securityContext->isGranted($attributes, $object);
        } catch (AuthenticationCredentialsNotFoundException $e) {
            return false;
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Generate complete roles attributes from the given string or array.
     *
     * @param string|array $attributes
     *
     * @return array
     */
    public function buildRolesAttributes($attributes)
    {
        if ( ! is_array($attributes)) {
            // find expected roles according to given action name
            $action = strtoupper($attributes);
            $attributes = isset($this->rolesPerActions[$action]) ? $this->rolesPerActions[$action] : array($action);
        }

        // build roles names
        if (count($attributes)) {
            foreach ($attributes as $key => $attribute) {
                $attributes[$key] = 'ROLE_' . $this->baseRoleName . (null !== $attribute ? '_' . strtoupper($attribute) : '');
            }
        } else {
            $attributes[] = 'ROLE_' . $this->baseRoleName;
        }

        return $attributes;
    }
}
