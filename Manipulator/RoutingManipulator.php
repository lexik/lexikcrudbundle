<?php
namespace Lexik\Bundle\CrudBundle\Manipulator;

/**
 * Add the YAML definitions of a crud route to YAML config file
 *
 * @author Laurent Heurtault <l.heurtault@lexikteam.com>
 */
class RoutingManipulator
{
    private $file;

    /**
     * Constructor.
     *
     * @param string $file The YAML routing config file path
     */
    public function __construct($file)
    {
        $this->file = $file;
    }

     /**
     * Adds a routing resource to a routing config file
     *
     * @param string $routeName full route name
     * @param string $controllerResource resource definition of the crud controller
     *
     * @return Boolean true if it worked, false otherwise
     *
     * @throws \RuntimeException If bundle is already imported
     */
    public function addRoute($routeName, $controllerResource)
    {
        if (!file_exists($this->file)) {
            throw new \RuntimeException(sprintf('Routing configuration file "%s" not found.', $this->file));
        }

        $current = file_get_contents($this->file);

        // Don't add same route twice
        if (false !== strpos($current, $routeName)) {
            throw new \RuntimeException(sprintf('Route "%s" already declared in "%s".', $routeName, $this->file));
        }

        $definition = <<<EOF
$routeName:
    resource: "$controllerResource"
    type:     lexikcrud
    prefix:   /

EOF;

        if (false === file_put_contents($this->file, $current.$definition)) {
            return false;
        }

        return true;
    }
}
