<?php

namespace Lexik\Bundle\CrudBundle\Tests\Form\Type;

use Lexik\Bundle\CrudBundle\Tests\TestCase;

use Symfony\Component\Form\FormFactory;
use Symfony\Component\Form\Extension\Core\CoreExtension;
use Symfony\Component\Form\ResolvedFormTypeFactory;
use Symfony\Component\Form\FormRegistry;

use Lexik\Bundle\CrudBundle\Form\Extension\CrudExtension;
use Lexik\Bundle\CrudBundle\Form\Type\VatChoicerType;

/**
 * Unit test for VatChoicerType form type.
 *
 * @package LexikCrudBundle
 * @author  Bérenger VIDAL <b.vidal@lexik.fr>
 */
class VatChoicerTypeTest extends TestCase
{
    /**
     * @var Symfony\Component\Form\FormFactory
     */
    protected $factory;

    protected function setUp()
    {
        parent::setUp();

        // init form factory
        $resolvedFormTypeFactory = new ResolvedFormTypeFactory();

        $this->registry = new FormRegistry(array(
            new CoreExtension(),
            new CrudExtension(),
        ), $resolvedFormTypeFactory);

        $this->factory = new FormFactory($this->registry, $resolvedFormTypeFactory);
    }

    protected function tearDown()
    {
        $this->factory = null;
    }

    public function testDefaultChoices()
    {
        $defaultChoices = array(
            '0'    => '0%',
            '5.5'  => '5.5%',
            '19.6' => '19.6%',
        );

        $field = $this->factory->createNamed('lexik_vat_choicer', 'test_lexik_vat_choicer', null, array(
            'required' => false,
        ));

        $this->assertEquals($defaultChoices, $field->createView()->get('choices'));
    }

    public function testCustomChoices()
    {
        $customChoices = array(
            '7.42'  => '7.42',
            '14.02' => '14,02',
            '19.85' => '19.85',
        );

        $field = $this->factory->createNamed('lexik_vat_choicer', 'test_lexik_vat_choicer', null, array(
            'required' => false,
            'choices'  => $customChoices,
        ));

        $this->assertEquals($customChoices, $field->createView()->get('choices'));
    }

    public function testBindSingleChoice()
    {
        $form = $this->factory->create('lexik_vat_choicer', null, array(
            'required' => false,
        ));

        $form->bind('5.50');
        $this->assertEquals(5.50, $form->getData());
        $this->assertEquals(5.50, $form->getClientData());

        $form->bind(5.50);
        $this->assertEquals(5.50, $form->getData());
        $this->assertEquals(5.50, $form->getClientData());
    }
}
