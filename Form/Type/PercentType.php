<?php

namespace Lexik\Bundle\CrudBundle\Form\Type;

use Symfony\Component\Form\AbstractType;

/**
 * A very simple percent widget without any NumberFormatter interaction.
 *
 * @package LexikCrudBundle
 * @author  Jeremy Barthe <j.barthe@lexik.fr>
 */
class PercentType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function getParent()
    {
        return 'text';
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'lexik_percent';
    }
}
