<?php

namespace Lexik\Bundle\CrudBundle\Tests\Fixtures;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class ArticleManyIdentifier
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     */
    private $id1;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     */
    private $id2;
}
