<?php

namespace Lexik\Bundle\CrudBundle\Form\Type;

use Lexik\Bundle\CrudBundle\Form\DataTransformer\EntityToIdentifierTransformer;

use Symfony\Bridge\Doctrine\ManagerRegistry;
use Symfony\Bridge\Doctrine\Form\EventListener\MergeDoctrineCollectionListener;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\PropertyAccess\PropertyPath;
use Symfony\Component\OptionsResolver\Options;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\Form\FormInterface;

/**
 * EntityChoicerType class is similar to EntityType except that it does not
 * take about all the collection, only about the given value
 * from an autocomplete or modalbox widget.
 *
 * @package LexikCrudBundle
 * @author  Jeremy Barthe <j.barthe@lexik.fr>
 * @author  Cédric Girard <c.girard@lexik.fr>
 */
class EntityChoicerType extends AbstractType
{
    /**
     * @var ManagerRegistry
     */
    protected $registry;

    /**
     * Construct.
     *
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        $this->registry = $registry;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        if ($options['multiple']) {
            $builder->addEventSubscriber(new MergeDoctrineCollectionListener());
        }

        $builder->addViewTransformer(new EntityToIdentifierTransformer($options['em'], $options['class'], $options['multiple']), true);
    }

    /**
     * {@inheritdoc}
     */
    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        $view->vars['multiple'] = $options['multiple'];

        if ($options['multiple']) {
            $view->vars['full_name'] = $view->vars['full_name'].'[]';
        }

        if ($options['multiple']) {
            $view->vars['data_label'] = array();

            foreach ($view->vars['data'] as $entity) {
                $view->vars['data_label'][$entity->getId()] = $this->generateLabel($entity, $options['property']);
            }
        } else {
            $view->vars['data_label'] = $this->generateLabel($view->vars['data'], $options['property']);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        parent::setDefaultOptions($resolver);

        $registry = $this->registry;

        $emNormalizer = function (Options $options, $em) use ($registry) {
            return (null !== $em) ? $registry->getManager($em) : $registry->getManagerForClass($options['class']);
        };

        $resolver
            ->setDefaults(array(
                'class'    => null,
                'property' => null,
                'em'       => null,
                'multiple' => false,
            ))
            ->setRequired(array(
                'class',
            ))
            ->setNormalizers(array(
                'em' => $emNormalizer,
            ));
    }

    /**
     * {@inheritdoc}
     */
    public function getParent()
    {
        return 'text';
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'lexik_entity_choicer';
    }

    /**
     * Generate a string label for the given object.
     *
     * @param object $object
     * @param string $property
     * @return string
     */
    private function generateLabel($object, $property)
    {
        $label = '';

        if (null !== $property && is_object($object)) {
            $accessor = PropertyAccess::getPropertyAccessor();
            $label = $accessor->getValue($object, new PropertyPath($property));
        } else {
            $label = (string) $object;
        }

        return $label;
    }
}
