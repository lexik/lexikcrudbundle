/**
 * Datepicker jQuery plugin based on Symfony2 date type.
 *
 * Depends:
 *     jQuery
 *     jQuery UI
 *     jQuery UI ThemeRoller
 *
 * @author Jeremy Barthe <j.barthe@lexik.fr>
 */

(function($){
    $.fn.lxDatepickerWidget = function(options){

        var defaults = {}
        var settings = $.extend(defaults, options);

        function init() {
            var self = $(this);

            var baseId = self.attr('id').slice(0, -4);

            // update datepicker when change one of selects
            $('#' + baseId + 'day, #' + baseId + 'month, #' + baseId + 'year').bind('change', function() {
                var baseId = this.id.slice(0, this.id.lastIndexOf('_')) + '_';
                updateDatepicker(baseId);
            });

            // calculate date ranges for datepicker
            var minYear = $(this).children(':eq(1)').val();
            var maxYear = $(this).children(':last').val();
            var minY = minYear > maxYear ? maxYear : minYear;
            var maxY = minYear < maxYear ? maxYear : minYear;

            $('<input type="hidden" class="lx-widget-datepicker-trigger" id="' + baseId + 'datepicker">')
                .insertAfter(self)
                .datepicker({
                    showOn: 'button',
                    buttonImage: '/bundles/lexikcrud/img/icons/calendar.png',
                    buttonImageOnly: true,
                    dateFormat: 'mm/dd/yy',
                    changeMonth: true,
                    changeYear: true,
                    minDate: new Date(minY, 1, 1),
                    maxDate: new Date(maxY, 11, 31),
                    onSelect: function(date, inst) {
                        updateDateWidget(baseId, date)
                    }
                });
        }

        function updateDatepicker(baseId) {
            var year  = $('#' + baseId + 'year').val();
            var month = $('#' + baseId + 'month').val();
            var day   = $('#' + baseId + 'day').val();

            if (year && month && day) {
                var date = new Date(year, month - 1, day);
                $('#datepick_' + baseId).datepicker('option', 'defaultDate', date);
            }
        }

        function updateDateWidget(baseId, selectedDate) {
            var date = new Date(selectedDate);
            $('#' + baseId + 'day option[value=' + date.getDate() + ']').attr('selected', 'selected');
            $('#' + baseId + 'month option[value=' + (date.getMonth() + 1) + ']').attr('selected', 'selected');
            $('#' + baseId + 'year option[value=' + (date.getFullYear()) + ']').attr('selected', 'selected').trigger('change');
        }

        $(this).each(init);

        return $(this);
    };

    $(function(){
        $('select[id$=year]:not(.no-datepicker)').lxDatepickerWidget();
    });
})(jQuery);
