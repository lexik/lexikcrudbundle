<?php

namespace Lexik\Bundle\CrudBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 *
 * @package LexikCrudBundle
 * @author  Jeremy Barthe <j.barthe@lexik.fr>
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritDoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('lexik_crud');

        $defaultActions               = $this->getDefaultActions();
        $defaultTemplates             = $this->getDefaultTemplates();
        $defaultTemplateNamesByAction = $this->getDefaultTemplatesNamesByAction();
        $defaultServiceIds            = $this->getDefaultServiceIds();

        $rootNode
            ->addDefaultsIfNotSet()
            ->children()
                ->scalarNode('base_template')
                    ->cannotBeEmpty()
                    ->defaultValue('LexikCrudBundle::layout.html.twig')
                ->end()
                ->scalarNode('base_ajax_template')
                    ->cannotBeEmpty()
                    ->defaultValue('LexikCrudBundle::ajax_layout.html.twig')
                ->end()
                ->scalarNode('pagerfanta_view')
                    ->cannotBeEmpty()
                    ->defaultValue('lexik_translated')
                ->end()

                ->arrayNode('default_actions')
                    ->defaultValue($defaultActions)
                    ->useAttributeAsKey('name')
                    ->prototype('array')
                        ->children()
                            ->scalarNode('pattern')->isRequired()->end()
                            ->arrayNode('defaults')->isRequired()->end()
                            ->arrayNode('requirements')->isRequired()->end()
                        ->end()
                    ->end()
                    ->beforeNormalization()
                        ->ifArray()
                        ->then(function($values) use ($defaultActions) {
                            return array_merge($defaultActions, $values);
                        })
                    ->end()
                ->end()

                ->arrayNode('default_templates')
                    ->defaultValue($defaultTemplates)
                    ->useAttributeAsKey('name')
                    ->prototype('scalar')
                    ->end()
                    ->beforeNormalization()
                        ->ifArray()
                        ->then(function($values) use ($defaultTemplates) {
                            return array_merge($defaultTemplates, $values);
                        })
                    ->end()
                ->end()

                ->arrayNode('default_template_names_by_action')
                    ->defaultValue($defaultTemplateNamesByAction)
                    ->useAttributeAsKey('name')
                    ->prototype('scalar')
                    ->end()
                    ->beforeNormalization()
                        ->ifArray()
                        ->then(function($values) use ($defaultTemplateNamesByAction) {
                            return array_merge($defaultTemplateNamesByAction, $values);
                        })
                    ->end()
                ->end()

                ->arrayNode('default_service_ids')
                    ->defaultValue($defaultServiceIds)
                    ->useAttributeAsKey('name')
                    ->prototype('scalar')
                    ->end()
                    ->beforeNormalization()
                        ->ifArray()
                        ->then(function($values) use ($defaultServiceIds) {
                            return array_merge($defaultServiceIds, $values);
                        })
                    ->end()
                ->end()
            ->end()
        ;

        return $treeBuilder;
    }

    /**
     * Return some additional service ids.
     *
     * @return array
     */
    public function getDefaultServiceIds()
    {
        return array(
            'filter_manager'    => 'lexik_crud.filter_manager',
            'template_selector' => 'lexik_crud.template_selector',
            'route_generator'   => 'lexik.crud.route_generator',
        );
    }

    /**
     * Return default templates.
     *
     * @return array
     */
    protected function getDefaultTemplates()
    {
        return array(
            'list' => 'LexikCrudBundle:CRUD:list.html.twig',
            'new'  => 'LexikCrudBundle:CRUD:form.html.twig',
            'edit' => 'LexikCrudBundle:CRUD:form.html.twig',
            'show' => 'LexikCrudBundle:CRUD:show.html.twig',
        );
    }

    /**
     * Return default template names by actions.
     *
     * @return array
     */
    protected function getDefaultTemplatesNamesByAction()
    {
        return array(
            'list' => 'list',
            'new'  => 'form',
            'edit' => 'form',
            'show' => 'show',
        );
    }

    /**
     * Return default route actions.
     *
     * @return array
     */
    protected function getDefaultActions()
    {
        return array(
            'list' => array(
                'path'         => '/',
                'defaults'     => array(),
                'requirements' => array(),
            ),
            'reset' => array(
                'path'         => '/reset',
                'defaults'     => array(),
                'requirements' => array(),
            ),
            'batch' => array(
                'path'         => '/batch',
                'defaults'     => array(),
                'requirements' => array(),
            ),
            'show' => array(
                'path'         => '/{id}/show',
                'defaults'     => array(),
                'requirements' => array(),
            ),
            'new' => array(
                'path'         => '/new',
                'defaults'     => array(),
                'requirements' => array(),
            ),
            'edit' => array(
                'path'         => '/{id}/edit',
                'defaults'     => array(),
                'requirements' => array(),
            ),
            'delete' => array(
                'path'         => '/{id}/delete',
                'defaults'     => array(),
                'requirements' => array(),
            ),
        );
    }
}
