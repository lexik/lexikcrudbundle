/**
 * Form prototype jQuery plugin based on Symfony2 form collection prototype attribute.
 *
 * Depends:
 *     jQuery
 *
 * @author Jeremy Barthe <j.barthe@lexik.fr>
 */

(function($){
    $.fn.lxFormPrototype = function(options){

        var defaults = {
            containerElement: '.lx-widget-form-prototype-container',
            firstLineClassName: 'lx-widget-form-prototype-first-line',
            containerItemElement: 'tr,div,li',
            linkAddElement: '.lx-widget-form-prototype-add',
            linkDeleteElement: '.lx-widget-form-prototype-delete'
        }

        var settings = $.extend(defaults, options);

        var addElement = function(container) {
            var index = container.children(settings.containerItemElement).length;
            var newElement = $(container.attr('data-prototype').replace(/__name__/g, index));
            container.append(newElement);
            container.trigger('form.prototype.add', [newElement]);
        }

        function init(){
            var self = $(this);

            var container = $(settings.containerElement, self);

            if (container.children().length == 0 && self.hasClass(settings.firstLineClassName)) {
                addElement(container);
            }

            $(settings.linkAddElement, self).bind('click', function(e) {
                e.preventDefault();
                addElement(container);
            });

            $(settings.containerElement, self).on('click', settings.linkDeleteElement, function(e) {
                e.preventDefault();
                var element = $(this).closest(settings.containerItemElement);
                container.trigger('form.prototype.delete', [$(element)]);
                $(this).closest(element).remove();
            });
        }

        $(this).each(init);

        return $(this);
    };

    $(function(){
        $('.lx-widget-form-prototype').lxFormPrototype();
    });
})(jQuery);
