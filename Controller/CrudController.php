<?php

namespace Lexik\Bundle\CrudBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\Form\Form;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

use Lexik\Bundle\CrudBundle\Security\Handler\SecurityHandlerInterface;
use Lexik\Bundle\CrudBundle\Filter\FilterManager;

/**
 * CRUD controller.
 *
 * @package LexikCrudBundle
 * @author  Jeremy Barthe <j.barthe@lexik.fr>
 */
class CrudController extends Controller
{
    /**
     * @var Lexik\Bundle\CrudBundle\Filter\FilterManager
     */
    protected $filterManager;

    /**
     * @var Lexik\Bundle\CrudBundle\Template\TemplateSelector
     */
    protected $templateSelector;

    /**
     * @var Lexik\Bundle\CrudBundle\Routing\RouteGenerator
     */
    protected $routeGenerator;

    /**
     * @var SecurityHandlerInterface
     */
    protected $securityHandler;

    /**
     * Entity name attached to this CRUD.
     *
     * @var string
     */
    protected $entityName;

    /**
     * Base route name for routing generation (defined as annotation).
     *
     * @var string
     */
    protected $baseRouteName;

    /**
     * Service ID (defined as annotation).
     *
     * @var string
     */
    protected $serviceId;

    /**
     * Resolved CRUD controller options.
     *
     * @var array
     */
    protected $options;

    /**
     * Constructor.
     *
     * @param ContainerInterface       $container
     * @param string                   $entityName
     * @param array                    $serviceIds
     * @param SecurityHandlerInterface $securityHandler
     * @param string                   $baseRoleName
     */
    public function __construct(ContainerInterface $container, $entityName, array $serviceIds = array(), SecurityHandlerInterface $securityHandler = null, $baseRoleName = null)
    {
        $this->container  = $container;
        $this->entityName = $entityName;

        // annotation informations
        $annot = $this->get('annotation_reader')
                      ->getClassAnnotation(new \ReflectionClass(get_class($this)), 'Lexik\\Bundle\\CrudBundle\\Routing\\Route');

        $this->baseRouteName = $annot->getName();
        $this->serviceId     = $annot->getServiceId();

        $optionsResolver = new OptionsResolver();
        $optionsResolver->setDefaults(array(
            'service_ids'           => $serviceIds, // Ids of services used by the CrudController class.
            'templates'             => array(),     // Templates name for each actions.
            'base_template'         => $this->container->getParameter('lexik_crud.base_template'),      // Layout name.
            'base_ajax_template'    => $this->container->getParameter('lexik_crud.base_ajax_template'), // Layout name in case of ajax call.
            'pagerfanta_view'       => $this->container->getParameter('lexik_crud.pagerfanta_view'),
            'list_default_sort'     => array('id', 'asc'),         // Default list sort.
            'max_per_page_choices'  => array(10, 20, 50, 100),     // Number of items per pages.
            'pager_type'            => FilterManager::PAGER_FANTA, // List pager type.
            'form_class_name'       => null,        // Form type class namespace.
            'edit_form_class_name'  => null,        // Form type class namespace only for edit page.
            'filter_class_name'     => null,        // Form filter type class namespace.
            'form_model_data_class' => null,        // Custom data class namespace
            'roles_per_actions'     => array(),     // Define an array of expected roles for each actions.
            'object_manager_id'     => 'doctrine.orm.entity_manager', // object manager service id
        ));

        $this->setDefaultOptions($optionsResolver);
        $this->options = $optionsResolver->resolve();

        // initialize services
        $serviceIds = $this->options['service_ids'];

        $this->createFilterManager($serviceIds['filter_manager']);
        $this->createTemplateSelector($serviceIds['template_selector']);
        $this->createRouteGenerator($serviceIds['route_generator']);

        // security handler
        if (null !== $securityHandler && null !== $baseRoleName) {
            $this->setSecurityHandler($securityHandler, $baseRoleName);
        }

        $this->configure();
    }

    /**
     * Set default CRUD Controller options.
     *
     * @param OptionsResolverInterface $resolver
     */
    protected function setDefaultOptions(OptionsResolverInterface $resolver)
    {
    }

    /**
     * Configure the CRUD Controller.
     */
    protected function configure()
    {
    }

    /**
     * Create FilterManager instance
     *
     * @param string $serviceId
     */
    protected function createFilterManager($serviceId)
    {
        $this->filterManager = $this->get($serviceId);
        $this->filterManager->setDefaultListParameters(array('sort' => $this->options['list_default_sort']));
        $this->filterManager->setStorageNamespace($this->baseRouteName);
        $this->filterManager->setRequest($this->getRequest());
    }

    /**
     * Create TemplateSelector instance
     *
     * @param string $serviceId
     */
    protected function createTemplateSelector($serviceId)
    {
        $this->templateSelector = $this->get($serviceId);
        $this->templateSelector->setRequest($this->getRequest());
        $this->templateSelector->setController($this);
        $this->templateSelector->setTemplates($this->options['templates']);
    }

    /**
     * Create RouteGenerator instance
     *
     * @param string $serviceId
     */
    protected function createRouteGenerator($serviceId)
    {
        $this->routeGenerator = $this->get($serviceId);
        $this->routeGenerator->setBaseRouteName($this->baseRouteName);
    }

    /**
     * Get an option by its name
     *
     * @param string $name
     * @return mixed
     */
    public function getOption($name)
    {
        if (!isset($this->options[$name])) {
            throw new \RuntimeException(sprintf('Unknown option "%s". Available options are: %s', $name, implode(', ', array_keys($this->options))));
        }

        return $this->options[$name];
    }

    /**
     * Returns the entity's namespace.
     *
     * @return string
     */
    public function getEntityName()
    {
        return $this->entityName;
    }

    /**
     * Returns the base toute name for this crud.
     *
     * @return string
     */
    public function getBaseRouteName()
    {
        return $this->baseRouteName;
    }

    /**
     * Returns the crud's route generator.
     *
     * @return \Lexik\Bundle\CrudBundle\Routing\RouteGenerator
     */
    public function getRouteGenerator()
    {
        return $this->routeGenerator;
    }

    /**
     * Returns the crud's filter manager.
     *
     * @return \Lexik\Bundle\CrudBundle\Filter\FilterManager
     */
    public function getFilterManager()
    {
        return $this->filterManager;
    }

    /**
     * Returns templates to use for crud actions.
     *
     * @return array
     */
    public function getTemplates()
    {
        return $this->options['templates'];
    }

    /**
     * Check if the current request is on XmlHttpRequest (ajax).
     *
     * @return boolean
     */
    protected function isXmlHttpRequest()
    {
        return $this->getRequest()->isXmlHttpRequest();
    }

    /**
     * Check if the current request is not master (sub-request).
     *
     * @return boolean
     */
    protected function isSubRequest()
    {
        return $this->getRequest()->getPathInfo() == '/_fragment';
    }

    /**
     * Return base template.
     *
     * @return string
     */
    public function getBaseTemplate()
    {
        return $this->isXmlHttpRequest() ? $this->getOption('base_ajax_template') : $this->getOption('base_template');
    }

    /**
     * Returns the Doctrine object manager to use.
     *
     * @return ObjectManager
     */
    public function getObjectManager()
    {
        return $this->get($this->options['object_manager_id']);
    }

    /**
     * Returns a query builder to use to build the list query.
     *
     * @param string $alias
     *
     * @return \Doctrine\ORM\QueryBuilder
     */
    protected function getListQueryBuilder($alias)
    {
        return $this->getObjectManager()
            ->getRepository($this->getEntityName())
            ->createQueryBuilder($alias);
    }

    /**
     * Create a new entioty instance.
     *
     * @return Object
     */
    protected function getNewEntity()
    {
        $class = $this->entityName;
        return new $class();
    }

    /**
     * Returns form options
     *
     * @param mixed $object
     *
     * @return array
     */
    protected function getFormOptions($object)
    {
        return array();
    }

    /**
     * Returns form filter options
     *
     * @return array
     */
    protected function getFormFilterOptions()
    {
        return array();
    }

    /**
     * Returns the right data model object to use with the form.
     *
     * @param object $entity
     * @return object
     */
    protected function getFormModel($entity)
    {
        if (null === $this->options['form_model_data_class']) {
            return $entity;
        }

        $dataClass = $this->options['form_model_data_class'];

        return new $dataClass($entity);
    }

    /**
     * Retruns a new intance of the form type.
     *
     * @return \Symfony\Component\Form\AbstractType
     *
     * @throws \RuntimeException
     */
    protected function getFormType()
    {
        if (null === $this->options['form_class_name']) {
            throw new \RuntimeException('You must specify a valid form class name');
        }

        if ($this->get('form.registry')->hasType($this->options['form_class_name'])) {
            return $this->options['form_class_name'];
        }

        $class = $this->options['form_class_name'];
        return new $class();
    }

    /**
     * Retruns a new intance of the form type for edit page.
     *
     * @param \Symfony\Component\Form\AbstractType
     *
     * @throws \RuntimeException
     */
    protected function getEditFormType($object)
    {
        $name = $this->options['edit_form_class_name'] ? $this->options['edit_form_class_name'] : $this->options['form_class_name'];

        if (null === $name) {
            throw new \RuntimeException('You must specify a valid form class name');
        }

        if ($this->get('form.registry')->hasType($name)) {
            return $name;
        }

        return new $name();
    }

    /**
     * Retruns a new intance of the form filter type.
     *
     * @return \Symfony\Component\Form\AbstractType|null
     */
    protected function getFormFilterType()
    {
        $formFilterType = null;

        if (null !== $this->options['filter_class_name']) {
            $name = $this->options['filter_class_name'];
            $formFilterType = $this->get('form.registry')->hasType($name) ? $name : new $name();
        }

        return $formFilterType;
    }

    /**
     * Returns all batch actions.
     *
     * @return array
     */
    public function getBatchActions()
    {
        return array(
            'delete' => $this->get('translator')->trans('list.batch_action.delete', array(), 'LexikCrudBundle'),
        );
    }

    /**
     * Delete batch action.
     *
     * @param array $ids
     */
    public function batchActionDelete(array $ids)
    {
        $deleted = 0;
        $em = $this->getObjectManager();

        $query = $em->createQuery(sprintf('SELECT o FROM %s o WHERE o.id IN (\'%s\')', $this->getEntityName(), implode("', '", $ids)));
        $iterableResult = $query->iterate();

        while ( ($row = $iterableResult->next()) !== false ) {
            if ($this->isObjectDeletable($row[0])) {
                $em->remove($row[0]);
                $deleted++;
            }
        }

        $em->flush();
        $em->clear();

        return $deleted;
    }

    /**
     * Returns true if the given object can be deleted.
     *
     * @param object $object
     */
    protected function isObjectDeletable($object)
    {
        return true;
    }

    /**
     * List entries with pagination.
     *
     * @return Response
     */
    public function listAction()
    {
        // check security authorization
        $this->checkCredentials('LIST');

        $alias = strtolower(substr($this->entityName, 0, 1)); // query alias

        $this->filterManager
             ->setQueryBuilder($this->getListQueryBuilder($alias))
             ->initializeFormFilter($this->getFormFilterType(), $this->getFormFilterOptions())
             ->addSortConditions()
             ->addFormFilterConditions();

        $formFilter = $this->filterManager->getFormFilter();

        return $this->render($this->templateSelector->getTemplate('list'), array_merge(array(
            'base_template'        => $this->getBaseTemplate(),
            'pagerfanta_view'      => $this->getOption('pagerfanta_view'),
            'is_xml_http_request'  => $this->isXmlHttpRequest(),
            'crud_name'            => $this->serviceId,
            'pager'                => $this->filterManager->createPager($this->options['pager_type']),
            'filter'               => ($formFilter instanceof Form) ? $formFilter->createView() : null,
            'batch_actions'        => $this->getBatchActions(),
            'max_per_page_choices' => $this->options['max_per_page_choices'],
            'show_pagination'      => ($this->options['pager_type'] === FilterManager::PAGER_FANTA),
        ), $this->getListExtraParameters()));
    }

    /**
     * Reset list filters.
     *
     * @return RedirectResponse
     */
    public function resetAction()
    {
        // check security authorization
        $this->checkCredentials('LIST');

        $this->filterManager->reset();

        return new RedirectResponse($this->routeGenerator->generateUrl('list'));
    }

    /**
     * Execute a batch action.
     *
     * @return RedirectResponse
     *
     * @throws \RuntimeException
     */
    public function batchAction()
    {
        // check security authorization
        $this->checkCredentials('BATCH');

        $action = $this->get('request')->get('action');
        $ids    = $this->get('request')->get('ids');

        if (count($ids) == 0) { // no item selected
            $this->get('session')->getFlashBag()->add('error', 'flash_batch_empty');

            return new RedirectResponse($this->routeGenerator->generateUrl('list', $this->filterManager->getListParameters()));
        }

        if ( ! array_key_exists($action, $this->getBatchActions())) {
            throw new \RuntimeException(sprintf('The `%s` batch action is not defined', $action));
        }

        $reflection = new \ReflectionClass(get_class($this));
        $batchActionName = sprintf('batchAction%s', Container::camelize($action));
        if ( ! $reflection->hasMethod($batchActionName)) {
            throw new \RuntimeException(sprintf('A `%s::%s` method must be created', get_class($this), $batchActionName));
        }

        $result = $this->$batchActionName($ids);
        if ($result instanceof Response) {
            return $result;
        } else {
            return new RedirectResponse($this->routeGenerator->generateUrl('list'));
        }
    }

    /**
     * Finds and displays a Post entity.
     *
     * @param int $id
     *
     * @return Response
     */
    public function showAction($id)
    {
        $object = $this->findObject($id);

        // check security authorization
        $this->checkCredentials('SHOW', $object);

        return $this->render($this->templateSelector->getTemplate('show'), array_merge(array(
            'base_template'       => $this->getBaseTemplate(),
            'is_xml_http_request' => $this->isXmlHttpRequest(),
            'crud_name'           => $this->serviceId,
            'object'              => $object,
        ), $this->getShowExtraParameters($object)));
    }

    /**
     * Displays a form to create a new entity.
     *
     * @return Response
     */
    public function newAction()
    {
        // check security authorization
        $this->checkCredentials('NEW');

        $object = $this->getNewEntity();
        $model  = $this->getFormModel($object);
        $form   = $this->createForm($this->getFormType(), $model, $this->getFormOptions($object));

        $request = $this->getRequest();
        if ($request->getMethod() == 'POST') {
            $form->bind($request);

            if ($form->isValid()) {
                $this->persist($model, $form);

                $this->get('session')->getFlashBag()->add('success', $this->get('translator')->trans('flash.create.success', array(), 'LexikCrudBundle'));

                return $this->redirectTo($object);
            }
        }

        return $this->render($this->templateSelector->getTemplate('new'), array_merge(array(
            'base_template'       => $this->getBaseTemplate(),
            'is_xml_http_request' => $this->isXmlHttpRequest(),
            'crud_name'           => $this->serviceId,
            'object'              => $object,
            'form'                => $form->createView(),
            'form_is_valid'       => $form->isBound() ? $form->isValid() : true,
        ), $this->getNewExtraParameters($object)));
    }

    /**
     * Persist entity.
     *
     * @param mixed $object
     * @param Form  $form
     */
    protected function persist($object, Form $form = null)
    {
        if (null !== $this->options['form_model_data_class'] && $object instanceof $this->options['form_model_data_class']) {
            throw new \RuntimeException('A custom form data class has been defined. Please override CrudController::persist() to correctly save the entity.');
        }

        $em = $this->getObjectManager();
        $em->persist($object);
        $em->flush();
    }

    /**
     * Displays a form to edit an existing Post entity.
     *
     * @return Response
     */
    public function editAction($id)
    {
        $em = $this->getObjectManager();

        $object = $this->findObject($id);

        // check security authorization
        $this->checkCredentials('EDIT', $object);

        $model = $this->getFormModel($object);
        $form = $this->createForm($this->getEditFormType($object), $model, $this->getFormOptions($object));

        $request = $this->getRequest();
        if ($request->getMethod() == 'POST') {
            $form->bind($request);

            if ($form->isValid()) {
                $this->update($model, $form);

                $this->get('session')->getFlashBag()->add('success', $this->get('translator')->trans('flash.update.success', array(), 'LexikCrudBundle'));

                return $this->redirectTo($object);
            }
        }

        return $this->render($this->templateSelector->getTemplate('edit'), array_merge(array(
            'base_template'       => $this->getBaseTemplate(),
            'is_xml_http_request' => $this->isXmlHttpRequest(),
            'crud_name'           => $this->serviceId,
            'object'              => $object,
            'form'                => $form->createView(),
            'form_is_valid'       => $form->isBound() ? $form->isValid() : true,
        ), $this->getEditExtraParameters($object)));
    }

    /**
     * Update entity.
     *
     * @param mixed $object
     * @param Form  $form
     */
    protected function update($object, Form $form = null)
    {
        if (null !== $this->options['form_model_data_class'] && $object instanceof $this->options['form_model_data_class']) {
            throw new \RuntimeException('A custom form data class has been defined. Please override CrudController::persist() to correctly save the entity.');
        }

        $em = $this->getObjectManager();
        $em->persist($object);
        $em->flush();
    }

    /**
     * Delete an object.
     *
     * @param int $id
     *
     * @return RedirectResponse
     */
    public function deleteAction($id)
    {
        $object = $this->findObject($id);

        // check security authorization
        $this->checkCredentials('DELETE', $object);

        if ($this->isObjectDeletable($object)) {
            $em = $this->getObjectManager();
            $em->remove($object);
            $em->flush();
            $em->clear();

            $this->get('session')->getFlashBag()->add('success', $this->get('translator')->trans('flash.delete.success', array(), 'LexikCrudBundle'));
        } else {
            $this->get('session')->getFlashBag()->add('success', $this->get('translator')->trans('flash.delete.not_possible', array(), 'LexikCrudBundle'));
        }

        return $this->redirectTo($object);
    }

    /**
     * Return some extra parameters to pass to the view.
     *
     * @return array
     */
    protected function getListExtraParameters()
    {
        return array();
    }

    /**
     * Return some extra parameters to pass to the view.
     *
     * @param object $object
     *
     * @return array
     */
    protected function getShowExtraParameters($object)
    {
        return array();
    }

    /**
     * Return some extra parameters to pass to the view.
     *
     * @param object $object
     *
     * @return array
     */
    protected function getNewExtraParameters($object)
    {
        return array();
    }

    /**
     * Return some extra parameters to pass to the view.
     *
     * @param object $object
     *
     * @return array
     */
    protected function getEditExtraParameters($object)
    {
        return array();
    }

    /**
     * Find an object by its id.
     *
     * @param int $id
     *
     * @return Object
     *
     * @throws NotFoundHttpException
     */
    protected function findObject($id)
    {
        $object = $this->getObjectManager()->find($this->entityName, $id);

        if ( ! $object) {
            throw new NotFoundHttpException(sprintf('Unable to find Entity "%s"', $this->entityName));
        }

        return $object;
    }

    /**
     * Refirect to the right page according to the previous action.
     *
     * @param Object $object
     *
     * @return RedirectResponse
     */
    protected function redirectTo($object)
    {
        $request = $this->getRequest();

        if (null !== $request->get('_submit_continue')) {
            $url = $this->routeGenerator->generateUrl('edit', array('id' => $object->getId()));
        } else if (null !== $request->get('_submit_add_another')) {
            $url = $this->routeGenerator->generateUrl('new');
        } else {
            $url = $this->routeGenerator->generateUrl('list');
        }

        return $this->redirect($url);
    }

    /**
     * Initialize security handler for CRUD.
     *
     * @param SecurityHandlerInterface $securityHandler
     * @param string                   $baseRoleName
     */
    public function setSecurityHandler(SecurityHandlerInterface $securityHandler, $baseRoleName)
    {
        $securityHandler->setBaseRoleName($baseRoleName);
        $securityHandler->mergeRolesPerActions($this->options['roles_per_actions']);

        $this->securityHandler = $securityHandler;
    }

    /**
     * Checks if the requested action is granted against the user roles.
     *
     * @param string      $action
     * @param null|object $object
     *
     * @return bool
     */
    public function isGranted($action, $object = null)
    {
        if ($this->securityHandler instanceof SecurityHandlerInterface) {
            return $this->securityHandler->isGranted($action, $object);
        }

        // by default, no security
        return true;
    }

    /**
     * Checks if the requested action is granted against the user roles, throws an exception otherwise.
     *
     * @param string      $action
     * @param null|object $object
     *
     * @throws AccessDeniedException
     */
    protected function checkCredentials($action, $object = null)
    {
        if ( ! $this->isGranted($action, $object)) {
            throw new AccessDeniedException(sprintf('Access denied due to lack role(s): %s or because you can\'t access to this object: %s', implode(', ', $this->securityHandler->buildRolesAttributes($action)), (string) $object));
        }
    }
}
