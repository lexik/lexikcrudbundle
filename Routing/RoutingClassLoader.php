<?php

namespace Lexik\Bundle\CrudBundle\Routing;

use Symfony\Component\Routing\Loader\AnnotationClassLoader;
use Symfony\Component\Config\Resource\FileResource;
use Symfony\Component\Routing\RouteCollection;
use Symfony\Component\Routing\Route as BaseRoute;

use Doctrine\Common\Annotations\Reader;

use Lexik\Bundle\CrudBundle\Routing\Route as CrudRoute;

/**
 * RoutingClassLoader loads CRUD routes collection. Example:
 *
 *     use Lexik\Bundle\CrudBundle\Routing\Route as CrudRoute;
 *
 *     /**
 *      * @CrudRoute("/demo", name="crud_demo", serviceId="my_project.crud_demo")
 *      * /
 *     class DemoController extends CrudController
 *     {
 *     }
 *
 * You can also overide default routes configuration like this:
 *
 *     /**
 *      * @CrudRoute("/demo", name="crud_demo", serviceId="my_project.crud_demo", options={
 *      *     "actions" = {
 *      *         "list" = {
 *      *             "path" = "/list"
 *      *         }
 *      *     }
 *      * })
 *      * /
 *
 * And finally you can unset some unused routes like this:
 *
 *     /**
 *      * @CrudRoute("/demo", name="crud_demo", serviceId="my_project.crud_demo", options={
 *      *     "actions" = {
 *      *         "batch"  = false,
 *      *         "delete" = false
 *      *     }
 *      * })
 *      * /
 *
 * @package LexikCrudBundle
 * @author  Jeremy Barthe <j.barthe@lexik.fr>
 */
class RoutingClassLoader extends AnnotationClassLoader
{
    /**
     * @var array
     */
    protected $actions = array();

    /**
     * Constructor.
     *
     * @param Reader $reader
     * @param array  $actions
     */
    public function __construct(Reader $reader, $actions)
    {
        parent::__construct($reader);

        $this->actions = $actions;
    }

    /**
     * Load routes from annotations from a class and from default CRUD actions.
     *
     * {@inheritdoc}
     */
    public function load($class, $type = null)
    {
        if (!class_exists($class)) {
            throw new \InvalidArgumentException(sprintf('Class "%s" does not exist.', $class));
        }

        $globals = array(
            'path'         => '',
            'requirements' => array(),
            'options'      => array(),
            'defaults'     => array(),
            'schemes'      => array(),
            'methods'      => array(),
            'host'         => '',
            'condition'    => null,
        );

        $class = new \ReflectionClass($class);
        if ($class->isAbstract()) {
            throw new \InvalidArgumentException(sprintf('Annotations from class "%s" cannot be read as it is abstract.', $class));
        }

        if ($annot = $this->reader->getClassAnnotation($class, $this->routeAnnotationClass)) {
            // for BC reasons
            if (null !== $annot->getPath()) {
                $globals['path'] = $annot->getPath();
            } elseif (null !== $annot->getPattern()) {
                $globals['path'] = $annot->getPattern();
            }

            if (null !== $annot->getRequirements()) {
                $globals['requirements'] = $annot->getRequirements();
            }

            if (null !== $annot->getOptions()) {
                $globals['options'] = $annot->getOptions();
            }

            if (null !== $annot->getDefaults()) {
                $globals['defaults'] = $annot->getDefaults();
            }

            if (null !== $annot->getSchemes()) {
                $globals['schemes'] = $annot->getSchemes();
            }

            if (null !== $annot->getMethods()) {
                $globals['methods'] = $annot->getMethods();
            }

            if (null !== $annot->getHost()) {
                $globals['host'] = $annot->getHost();
            }
        }

        $actions = $this->actions;

        // through options > actions you can override default routes configurations
        if (isset($globals['options']['actions']) && is_array($globals['options']['actions'])) {
            foreach ($globals['options']['actions'] as $action => $options) {
                if (is_array($options)) {
                    $actions[$action] = array_replace($actions[$action], $options);
                } else if ( ! $options) {
                    unset($actions[$action]);
                }
            }
        }

        // init route collection
        $collection = new RouteCollection();
        $collection->addResource(new FileResource($class->getFileName()));

        $baseRouteName = $annot->getName();
        $serviceId     = $annot->getServiceId();

        // load default CRUD routes
        foreach ($actions as $action => $options) {
            $method = $class->getMethod($action.'Action');
            $annots = $this->reader->getMethodAnnotations($method);

            if (count($annots) === 0) {
                $options = $actions[$action];

                $route = new CrudRoute(array(
                    'serviceId'    => $serviceId,
                    'name'         => sprintf('%s_%s', $baseRouteName, $action),
                    'path'         => $options['path'],
                    'host'         => isset($options['host']) ? $options['host'] : null,
                    'methods'      => isset($options['methods']) ? $options['methods'] : array(),
                    'schemes'      => isset($options['schemes']) ? $options['schemes'] : array(),
                    'requirements' => $options['requirements'],
                    'options'      => isset($options['options']) ? $options['options'] : array(),
                    'defaults'     => $options['defaults'],
                ));

                $annots[] = $route;
            }

            foreach ($annots as $annot) {
                if ($annot instanceof $this->routeAnnotationClass) {
                    $this->addRoute($collection, $annot, $globals, $class, $method);
                }
            }
        }

        // load routes from each methods
        foreach ($class->getMethods() as $method) {
            $this->defaultRouteIndex = 0;
            foreach ($this->reader->getMethodAnnotations($method) as $annot) {
                if ($annot instanceof $this->routeAnnotationClass) {
                    $this->addRoute($collection, $annot, $globals, $class, $method);
                }
            }
        }

        return $collection;
    }

    /**
     * Configure route with CRUD service name.
     *
     * {@inheritdoc}
     */
    protected function configureRoute(BaseRoute $route, \ReflectionClass $class, \ReflectionMethod $method, $annot)
    {
        // controller
        $annot = $this->reader->getClassAnnotation($class, $this->routeAnnotationClass);
        $route->setDefault('_controller', sprintf('%s:%s', $annot->getServiceId(), $method->getName()));
    }
}
