<?php

namespace Lexik\Bundle\CrudBundle\Security\Handler;

use Symfony\Component\Security\Core\SecurityContextInterface;

/**
 * Interface for security handler implementation.
 *
 * @package LexikCrudBundle
 * @author  Jeremy Barthe <j.barthe@lexik.fr>
 */
interface SecurityHandlerInterface
{
    /**
     * Checks if the attributes are granted against the current token.
     *
     * @abstract
     * @param string|array $attributes
     * @param null|object  $object
     *
     * @return boolean
     */
    public function isGranted($attributes, $object = null);

    /**
     * Set the base role name.
     *
     * @param string $baseRoleName
     */
    public function setBaseRoleName($baseRoleName);

    /**
     * Merge given roles per actions with default configuration.
     *
     * @param array $rolesPerActions
     */
    public function mergeRolesPerActions(array $rolesPerActions);
}
