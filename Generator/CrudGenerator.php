<?php

namespace Lexik\Bundle\CrudBundle\Generator;

use Doctrine\Common\Persistence\Mapping\ClassMetadata;
use Sensio\Bundle\GeneratorBundle\Generator\Generator;
use Symfony\Component\Filesystem\Filesystem;

use Lexik\Bundle\CrudBundle\Command\Validators;

/**
 * CrudGenerator class.
 *
 * @author Laurent Heurtault <l.heurtault@lexik.fr>
 * @author Olivier Maisonneuve <o.maisonneuve@lexik.fr>
 */
class CrudGenerator extends Generator
{
    /**
     * @var Filesystem
     */
    private $filesystem;

    /**
     * @var string
     */
    private $skeletonDir;

    /**
     * Constructor.
     *
     * @param Filesystem $filesystem
     * @param type       $skeletonDir
     */
    public function __construct(Filesystem $filesystem, $skeletonDir)
    {
        $this->filesystem  = $filesystem;
        $this->skeletonDir = $skeletonDir;
    }

    /**
     * Generate crud Controller and views.
     *
     * @param string        $entityShortcut
     * @param string        $bundlePath
     * @param string        $bundleNamespace
     * @param ClassMetadata $metadata
     *
     * @throws \RuntimeException
     */
    public function generate($entityShortcut, $bundlePath, $bundleNamespace, $metadata)
    {
        $formFilename = $this->getFormDirectory($bundlePath).Validators::generateCrudFormFilename($entityShortcut);

        if (file_exists($formFilename)) {
            throw new \RuntimeException(sprintf('The form "%s" already exists.', $formFilename));
        }

        $controllerFilename = $this->getControllerDirectory($bundlePath).Validators::generateCrudControllerFilename($entityShortcut);

        if (file_exists($controllerFilename)) {
            throw new \RuntimeException(sprintf('The controller "%s" already exists.', $controllerFilename));
        }

        $viewDirectory = $this->getViewDirectory(
            $bundlePath,
            Validators::getEntityName($entityShortcut)
        );

        $listViewFilename = $viewDirectory.'list.html.twig';

        if (file_exists($listViewFilename)) {
            throw new \RuntimeException(sprintf('The view "%s" already exists.', $listViewFilename));
        }

        $formViewFilename = $viewDirectory.'form.html.twig';

        if (file_exists($formViewFilename)) {
            throw new \RuntimeException(sprintf('The view "%s" already exists.', $formViewFilename));
        }

        $this->renderFile(
            $this->skeletonDir,
            'Form.php.twig',
            $formFilename,
            array(
                'bundleNamespace' => str_replace('\\\\', '\\', $bundleNamespace),
                'entityName'      => Validators::getEntityName($entityShortcut),
                'formName'        => strtolower(Validators::getEntityName($entityShortcut)),
                'fields'          => array_diff($metadata->getFieldNames(), $metadata->getIdentifier()),
            )
        );

        $this->renderFile(
            $this->skeletonDir,
            'Controller.php.twig',
            $controllerFilename,
            array(
                'bundleNamespace' => str_replace('\\\\', '\\', $bundleNamespace),
                'entityName'      => Validators::getEntityName($entityShortcut),
                'routePrefix'     => strtolower(Validators::getEntityName($entityShortcut)),
                'routeName'       => Validators::generateCrudRouteName($entityShortcut),
                'serviceId'       => Validators::generateCrudServiceId($entityShortcut),
            )
        );

        $this->renderFile(
            $this->skeletonDir,
            'list.twig.twig',
            $listViewFilename,
            array()
        );

        $this->renderFile(
            $this->skeletonDir,
            'form.twig.twig',
            $formViewFilename,
            array()
        );
    }

    /**
     * Return controller class's directory path and create it if it doesn't exists.
     *
     * @param string $bundlePath
     *
     * @return string
     */
    protected function getControllerDirectory($bundlePath)
    {
        $controllerDirectory = sprintf('%sController/', $bundlePath);

        if (!file_exists($controllerDirectory)) {
            $this->filesystem->mkdir($controllerDirectory);
        }

        return $controllerDirectory;
    }

    /**
     * Return controller class's directory path and create it if it doesn't exists.
     *
     * @param string $bundlePath
     *
     * @return string
     */
    protected function getFormDirectory($bundlePath)
    {
        $formDirectory = sprintf('%sForm/', $bundlePath);

        if (!file_exists($formDirectory)) {
            $this->filesystem->mkdir($formDirectory);
        }

        return $formDirectory;
    }

    /**
     * Return views' directory path and create it if it doesn't exists.
     *
     * @param string $bundlePath
     * @param string $entityName
     *
     * @return string
     */
    protected function getViewDirectory($bundlePath, $entityName)
    {
        $viewDirectory = sprintf('%sResources/views/%s/', $bundlePath, $entityName);

        if (!file_exists($viewDirectory)) {
            $this->filesystem->mkdir($viewDirectory);
        }

        return $viewDirectory;
    }
}
