/**
 * Autocomplete jQuery plugin based on Symfony2 form type.
 *
 * Depends:
 *     jQuery
 *     jQuery UI
 *     jQuery UI ThemeRoller
 *     mustache.js (optional)
 *
 * @author Jeremy Barthe <j.barthe@lexik.fr>
 */
(function($){
    $.lxAutocompleteWidget = {
        defaults: {
            minLength: 1,
            maxResults: null,
            i18n: {
                no_results: 'Aucun résultat',
                too_much_results: 'Trop de résultats'
            }
        }
    };

    $.fn.lxAutocompleteWidget = function(options){

        var settings = $.extend($.lxAutocompleteWidget.defaults, options);

        function init() {
            var self = $(this);

            var identifierColumn = self.attr('data-autocomplete-column-identifier') || 'id';
            var labelColumn      = self.attr('data-autocomplete-column-label') || 'label';
            var iteratorPath     = self.attr('data-autocomplete-iterator-path') || false;

            // multiple section
            var multiple         = self.attr('data-autocomplete-multiple') != undefined ? true : false;
            if (self.attr('data-autocomplete-template') != undefined) {
                var template = $('#' + self.attr('data-autocomplete-template')).html();
            }

            if (multiple) {
                var selectionListElement = self.find('.lx-widget-autocomplete-selection-list');
                var prototype = selectionListElement.attr('data-prototype');
                if (self.attr('data-autocomplete-prototype_template') != undefined) {
                    var prototypeTemplate = $('#' + self.attr('data-autocomplete-prototype_template')).html();
                }

                // bind delete selection on multiple autocomplete
                self.delegate('.lx-widget-autocomplete-selection-delete', 'click', function(e) {
                    e.preventDefault();
                    $(this).closest('li').fadeOut(function() {
                        $(this).remove();
                    });
                });
            }

            var autocompleteField = self.find('.lx-widget-autocomplete-choice-search');

            autocompleteField.on('updateAutocompleteField', function() {
                if (self.find('.lx-widget-autocomplete-choice-field').val()) {
                    autocompleteField
                        .removeClass('autocomplete-noselection')
                        .addClass('autocomplete-selected');
                } else {
                    autocompleteField
                        .removeClass('autocomplete-selected')
                        .addClass('autocomplete-noselection');
                }
            });

            var updateAutocompleteHelpMessage = function(length) {
                if (null != settings.maxResults && length > settings.maxResults) {
                    if (!autocompleteField.next().hasClass('tooltip')) {
                        autocompleteField.tooltip({
                            title: ''+ settings.i18n.too_much_results +'',
                            placement: 'right',
                            trigger: 'hover'
                        });
                        autocompleteField.tooltip('show');
                    }
                } else {
                    autocompleteField.tooltip('destroy');
                }
            };

            // autocomplete initialization
            autocompleteField.autocomplete({
                source: function(request, response) {
                    $.ajax({
                        cache: false,
                        context: autocompleteField,
                        url: self.attr('data-autocomplete-source'),
                        dataType: "json",
                        data: {
                            search: request.term
                        },
                        success: function(data) {
                            if (iteratorPath) {
                                data = data[iteratorPath];
                            }

                            if (data.length > 0) {
                                updateAutocompleteHelpMessage(data.length);

                                var index = 0;
                                response($.map(data, function(item) {
                                    index++;
                                    if (null == settings.maxResults || index <= settings.maxResults) {
                                        return {
                                            identifier : item[identifierColumn],
                                            label      : item[labelColumn],
                                            data       : item
                                        };
                                    }
                                }));
                            } else { // in case of no results
                                $(this).find('.lx-widget-autocomplete-choice-field').val('');
                                $(this).trigger('updateAutocompleteField');
                                autocompleteField.tooltip('destroy');

                                response([{
                                    identifier : '',
                                    label      : settings.i18n.no_results,
                                    data       : ''
                                }]);
                            }
                        },
                        beforeSend: function() {
                            $(this).addClass('autocomplete-loading');
                        },
                        complete: function() {
                            $(this).removeClass('autocomplete-loading');
                        }
                    });
                },
                minLength: self.attr('data-autocomplete-minlength') || settings.minLength,
                select: function(event, ui) {
                    if (multiple) { // append new row
                        if (prototypeTemplate != undefined) {
                            var html = Mustache.to_html(prototypeTemplate, ui.item);
                            selectionListElement.append(html);
                        } else {
                            var content = prototype
                                .replace(/\$\$id\$\$/g, ui.item.identifier)
                                .replace(/\$\$entity\$\$/g, ui.item.label);
                            selectionListElement.append(content);
                        }
                    } else {
                        self.find('.lx-widget-autocomplete-choice-field').val(ui.item.identifier);
                        $(this).trigger('updateAutocompleteField');
                    }
                },
                change: function() {
                    if (multiple) {
                        $(this).val('');
                    }
                }
            }).data('autocomplete')._renderItem = function(ul, item) {
                if (template) {
                    var html = Mustache.to_html(template, item);
                    return $(html)
                        .data('item.autocomplete', item)
                        .appendTo(ul);
                } else {
                    return $('<li></li>')
                        .data('item.autocomplete', item)
                        .append('<a>' + item.label + '</a>')
                        .appendTo(ul);
                }
            };

            autocompleteField.bind('blur', function(){
                if ($.trim($(this).val()) === '') {
                    $(this).find('.lx-widget-autocomplete-choice-field').val('');
                    $(this).trigger('updateAutocompleteField');
                }
            });

            // override clear event to unset hidden field value
            autocompleteField.bind('clear', function(){
                $(this).nextAll('input:hidden').val('');
                $(this).trigger('updateAutocompleteField');
            });

            $(this).trigger('updateAutocompleteField');
        }

        $(this).each(init);

        return $(this);
    };

    $(function(){
        $('.lx-widget-autocomplete-choice').lxAutocompleteWidget();
    });
})(jQuery);
