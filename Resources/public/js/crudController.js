/**
 * CRUD UX.
 *
 * Depends:
 *     jQuery
 *     jQuery UI
 *     jQuery UI ThemeRoller
 *     jquery.lxModalbox.js
 *
 * @author Jeremy Barthe <j.barthe@lexik.fr>
 */
var crudController = {
    init: function() {
        this.initTable();
        this.initFilter();
    },

    initTable: function() {
        // check all checkboxes when the one in a table head is checked
        $('thead .lx-listing-batch-checkbox').bind('click', function() {
            $(this).closest('table')
                   .find('tbody .lx-listing-batch-checkbox')
                   .attr('checked', $(this).is(':checked')).trigger('change');
        });

        // check corresponding checkbox when clicking on a row
        $('.lx-listing tbody tr').click(function(e) {
            if (e.target.nodeName != 'A' && e.target.nodeName != 'INPUT') {
                var checkbox = $(this).find('.lx-listing-batch-checkbox');
                $(checkbox).attr('checked', !$(checkbox).is(':checked')).trigger('change');
            }
        });

        // max per page select
        $('.lx-listing-max-per-page-select select').change(function() {
            $(this).closest('form').submit();
        });

        // confirm on delete link
        $('.lx-listing a.delete').bind('click', function() {
            return confirm($(this).attr('data-dialog-title') || $(this).attr('title'));
        });
    },

    initFilter: function() {
        $('.lx-filter-form').delegate('.lx-field-clear', 'click', function(e){
            $(this)
                .prevAll('input, textarea, select:not(.operator-selector)')
                .val('')
                .trigger('clear');
        });

        $('.lx-filter-form').each(function(){
            $(this).find('.controls').each(function() {
                var field = $(this).find('[type=text][value!=""], textarea:not(:empty), :checkbox:checked, :radio:checked, select:has(option:selected[value!=""]):not(.operator-selector)').first();
                if (field) {
                    $(field)
                        .addClass('lx-field-filled')
                        .after('<i class="lx-field-clear icon-remove-sign"></i>')
                        .bind('clear', function(){
                            $(this)
                                .removeClass('lx-field-filled')
                                .next('.lx-field-clear').remove();
                        });
                }
            });
        });
    }
};

$(function(){
    crudController.init();

    // datepicker
    if ($('.datepicker').length) {
        $('.datepicker').datepicker({
            showButtonPanel: true,
            changeMonth: true,
            changeYear: true
        });
    }

    // overlay clickout for dialogs
    $('body').on('click', '.ui-widget-overlay', function () {
        $(this).siblings('.ui-dialog').find('.ui-dialog-content').dialog('close');
    });
});
