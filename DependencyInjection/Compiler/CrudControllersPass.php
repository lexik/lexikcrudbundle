<?php

namespace Lexik\Bundle\CrudBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;

class CrudControllersPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container)
    {
        foreach ($container->findTaggedServiceIds('lexik_crud.controller') as $id => $attributes) {
            $index = 2;
            $definition = $container->findDefinition($id);

            try {
                $value = $definition->getArgument($index);
                if (empty($value)) {
                    $definition->replaceArgument($index, $container->getParameter('lexik.crud.default_service_ids'));
                }
            }
            catch (\OutOfBoundsException $exp) {
                $definition->addArgument($container->getParameter('lexik.crud.default_service_ids'));
            }
        }
    }
}