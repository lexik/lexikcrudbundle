<?php

namespace Lexik\Bundle\CrudBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;

/**
 * This is the class that loads and manages your bundle configuration
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html}
 *
 * @package LexikCrudBundle
 * @author  Jeremy Barthe <j.barthe@lexik.fr>
 */
class LexikCrudExtension extends Extension
{
    /**
     * {@inheritDoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $loader = new Loader\XmlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('crud.xml');
        $loader->load('form.xml');
        $loader->load('routing.xml');
        $loader->load('twig.xml');

        // default actions
        if ( ! is_array($config['default_actions']) || count($config['default_actions']) === 0) {
            throw new \InvalidArgumentException('The "default_actions" option must be filled');
        }

        // base templates
        $container->setParameter('lexik_crud.base_template', $config['base_template']);
        $container->setParameter('lexik_crud.base_ajax_template', $config['base_ajax_template']);
        $container->setParameter('lexik_crud.pagerfanta_view', $config['pagerfanta_view']);
        $container->setParameter('lexik_crud.routing_default_actions', $config['default_actions']);
        $container->setParameter('lexik_crud.default_templates', $config['default_templates']);
        $container->setParameter('lexik_crud.default_template_names_by_action', $config['default_template_names_by_action']);
        $container->setParameter('lexik.crud.default_service_ids', $config['default_service_ids']);
    }
}
