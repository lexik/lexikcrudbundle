<?php

namespace Lexik\Bundle\CrudBundle\Routing;

use Symfony\Component\Routing\Annotation\Route as BaseRoute;

/**
 * Route annotation class for CRUD.
 *
 * @Annotation
 * @package LexikCrudBundle
 * @author  Jeremy Barthe <j.barthe@lexik.fr>
 */
class Route extends BaseRoute
{
    /**
     * Service ID.
     *
     * @var string
     */
    protected $serviceId;

    public function setServiceId($serviceId)
    {
        $this->serviceId = $serviceId;
    }

    public function getServiceId()
    {
        return $this->serviceId;
    }
}
