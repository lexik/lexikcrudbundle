/**
 * Modalbox choice jQuery plugin based on Symfony2 form type.
 *
 * Depends:
 *     jQuery
 *     jQuery UI
 *     jQuery UI ThemeRoller
 *     jquery.form.js
 *
 * @author Jeremy Barthe <j.barthe@lexik.fr>
 */

(function($){
    $.fn.lxModalboxChoiceWidget = function(options){

        var defaults = {
            linkElement: '.lx-widget-modalbox-choice-select',
            textElement: '.lx-widget-modalbox-choice-text',
            deleteElement: '.lx-widget-modalbox-choice-delete',
            fieldElement: '.lx-widget-modalbox-choice-field',
            dialogIdentifierElement: '.identifier',
            dialogIdentifierAttribute: 'data-identifier',
            dialogElementPrefix: 'lx-widget-modalbox-dialog',
            dialogOptions: {
                autoOpen: false,
                width:    950,
                height:   500,
                modal:    true
            }
        };

        var dialogElement;

        var settings = $.extend(defaults, options);

        var listenToClickOnLinkElement = function(e, href) {
            e.preventDefault();

            // validate URL
            if ((typeof href === 'undefined') && (
                $.trim($(this).attr('href')) === ''
                || $(this).attr('href') === '#'
                || $(this).attr('href') === 'javascript:void(0)'
            )) {
                return;
            }

            var dialog = getUiDialog();

            // add loader
            dialog
                .empty()
                .addClass('loader');

            $.ajax({
                type: 'GET',
                cache: false,
                url: $(this).attr('href') || href,
                success: function(html) {
                    renderHtml(html, dialog);
                }
            });
        }

        var listenToClickOnDialogIdentifierElement = function(e) {
            e.preventDefault();

            var dialog = getUiDialog();

            var opener = dialog.data('opener');

            $(settings.fieldElement, opener)
                .val($(this).attr(settings.dialogIdentifierAttribute));

            $(settings.textElement, opener)
                .text($(this).text());

            // custom event: pick an element from modalbox
            $(settings.fieldElement, opener).trigger('modalboxchoice.select', [dialog, $(this).attr(settings.dialogIdentifierAttribute), $(this)]);

            dialog.dialog('close');
        }

        var listenToSubmitOnFormElement = function(e) {
            e.preventDefault();

            var form = $(this);
            var dialog = getUiDialog();

            // add loader
            dialog
                .empty()
                .addClass('loader');

            $(form).ajaxSubmit({
                type: form.attr('method'),
                url:  form.attr('action'),
                data: { _xml_http_request: true },
                success: function(html) {
                    renderHtml(html, dialog);
                }
            });
        }

        var renderHtml = function(html, dialog) {
            dialog.html(html);

            // cascading dialog box
            dialog.find('.lx-widget-modalbox-choice').lxModalboxChoiceWidget();

            // select an element on dialog box
            dialog.find(settings.dialogIdentifierElement).bind('click', listenToClickOnDialogIdentifierElement);

            // click on dialog box
            dialog.find('a:not([data-dialog]):not(' + settings.dialogIdentifierElement + '):not(' + settings.linkElement + ')').bind('click', listenToClickOnLinkElement);

            // submit form on dialog box
            dialog.find('form').bind('submit', listenToSubmitOnFormElement);

            // remove loader
            dialog.removeClass('loader');

            if ( ! dialog.dialog('isOpen')) {
                dialog.dialog('open');
            }
        }

        var getUiDialog = function() {
            var dialog = $(dialogElement);
            if (dialog.length == 0) {
                var dialog = $('<div id="' + dialogElement.replace('#', '') + '" />').appendTo(document);
                dialog.data('opener', null);

                return dialog.dialog(settings.dialogOptions);
            }

            return dialog;
        }

        var getUniqueID = function() {
            var uniqueID = new Date();
            return uniqueID.getTime();
        }

        function init() {
            var self = $(this);

            dialogElement = '#' + settings.dialogElementPrefix + getUniqueID();

            // delete link: only if field isn't required
            var isRequired = self.find(settings.fieldElement).attr('required');
            if (isRequired == undefined || isRequired == '') {
                self.find(settings.deleteElement)
                    .show()
                    .bind('click', function(e) {
                        e.preventDefault();

                        $(settings.fieldElement, self).val('');
                        $(settings.textElement, self).text('');
                    });
            }

            // open dialog box
            self.find(settings.linkElement).bind('click', function(e) {
                e.preventDefault();

                var dialog = getUiDialog();

                dialog.data('opener', self); // store opener
                var href = $(this).attr('data-href') || $(this).attr('href');
                listenToClickOnLinkElement(e, href);

                // custom event: dialog box's opened for the first time
                self.trigger('dialog.open', [dialog, self]);

                return false;
            });
        }

        $(this).each(init);

        return $(this);
    };

    $(function(){
        $('.lx-widget-modalbox-choice').lxModalboxChoiceWidget();
    });
})(jQuery);
