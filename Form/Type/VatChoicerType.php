<?php

namespace Lexik\Bundle\CrudBundle\Form\Type;

use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\AbstractType;

/**
 * VAT Select Choice
 *
 * @package LexikCrudBundle
 * @author  Bérenger VIDAL <b.vidal@lexik.fr>
 */
class VatChoicerType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function getParent()
    {
        return 'choice';
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        parent::setDefaultOptions($resolver);

        $resolver->setDefaults(array(
            'choices' => array(
                '0'    => '0%',
                '5.5'  => '5.5%',
                '19.6' => '19.6%',
            ),
            'empty_value' => false,
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'lexik_vat_choicer';
    }
}
