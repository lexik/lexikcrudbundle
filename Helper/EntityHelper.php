<?php

namespace Lexik\Bundle\CrudBundle\Helper;

use Doctrine\ORM\EntityManager;

/**
 * Entity helper.
 *
 * @package LexikCrudBundle
 * @author  Jeremy Barthe <j.barthe@lexik.fr>
 * @author  Cédric Girard <c.girard@lexik.fr>
 */
class EntityHelper
{
    /**
     * @var Doctrine\ORM\EntityManager
     */
    private $em;

    /**
     * @var Doctrine\ORM\UnitOfWork
     */
    private $unitOfWork;

    /**
     * @var string
     */
    private $entityName;

    /**
     * @var string
     */
    private $identifierName;

    /**
     * Constructor.
     *
     * @param EntityManager $em
     * @param string        $entityName
     */
    public function __construct(EntityManager $em, $entityName)
    {
        $this->em         = $em;
        $this->unitOfWork = $this->em->getUnitOfWork();
        $this->entityName = $entityName;

        $this->identifierName = $this->em->getClassMetadata($entityName)->getSingleIdentifierFieldName();
    }

    /**
     * Returns the identifier name.
     *
     * @return string
     */
    public function getIdentifierName()
    {
        return $this->identifierName;
    }

    /**
     * Retrieves entities from an array of ids.
     *
     * @param array $ids
     *
     * @return array
     */
    public function getEntitiesByIds(array $ids)
    {
        $results = array();

        if (count($ids) > 0) {
            $qb = $this->em
                 ->getRepository($this->entityName)
                 ->createQueryBuilder('o');

            $results = $qb->where($qb->expr()->in('o.' . $this->identifierName, $ids))
                ->getQuery()
                ->getResult();
        }

        return $results;
    }

    /**
     * Returns an entity by its id.
     *
     * @param int $id
     * @return object
     */
    public function getEntityById($id)
    {
        return $this->em->find($this->entityName, $id);
    }

    /**
     * Return identifier of a given entity object.
     *
     * @param object $entity
     *
     * @return mixed
     */
    public function getEntityIdentifier($entity)
    {
        $values = $this->getIdentifierValues($entity);

        return isset($values[$this->identifierName]) ? $values[$this->identifierName] : null;
    }

    /**
     * Return all identifier(s) of a given entity object.
     *
     * @param object $entity
     *
     * @return mixed
     *
     * @throws InvalidArgumentException
     */
    public function getIdentifierValues($entity)
    {
        if ( ! $this->unitOfWork->isInIdentityMap($entity) ) {
            throw new \InvalidArgumentException('Object passed to the entity helper must be managed.');
        }

        return $this->unitOfWork->getEntityIdentifier($entity);
    }
}
