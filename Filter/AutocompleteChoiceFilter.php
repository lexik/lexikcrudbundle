<?php

namespace Lexik\Bundle\CrudBundle\Filter;

use Doctrine\ORM\QueryBuilder;

use Lexik\Bundle\FormFilterBundle\Filter\ORM\ORMFilter;
use Lexik\Bundle\FormFilterBundle\Filter\ORM\Expr;

/**
 * @package LexikCrudBundle
 * @author  Jeremy Barthe <j.barthe@lexik.fr>
 */
class AutocompleteChoiceFilter extends ORMFilter
{
    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'lexik_filter_autocomplete_choice';
    }

    /**
     * {@inheritdoc}
     */
    protected function apply(QueryBuilder $queryBuilder, Expr $expr, $field, array $values)
    {
        if (!empty($values['value'])) {
            $paramName = sprintf('%s_param', str_replace('.', '_', $field));

            $queryBuilder
                ->andWhere(sprintf('%s = :%s', $field, $paramName))
                ->setParameter($paramName, $values['value'], \PDO::PARAM_STR);
        }
    }
}
