<?php

namespace Lexik\Bundle\CrudBundle\Filter;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormFactory;

use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\Query;

use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\Pagerfanta;
use Pagerfanta\Exception\NotValidCurrentPageException;

use Lexik\Bundle\FormFilterBundle\Filter\FilterBuilderUpdaterInterface;
use Lexik\Bundle\CrudBundle\Pagerfanta\SinglePagePager;

class FilterManager
{
    /**
     * Pager types
     */
    const PAGER_FANTA       = 11;
    const PAGER_SINGLE_PAGE = 12;

    /**
     * @var \Symfony\Component\Form\FormFactory
     */
    protected $formFactory;

    /**
     * @var \Symfony\Component\Form\Form
     */
    protected $formFilter;

    /**
     * @var FilterBuilderUpdaterInterface
     */
    protected $filterQueryBuilder;

    /**
     * @var \Doctrine\ORM\QueryBuilder
     */
    protected $queryBuilder;

    /**
     * @var \Symfony\Component\HttpFoundation\Request
     */
    protected $request;

    /**
     * @var string
     */
    protected $storageNamespace;

    /**
     * @var array
     */
    protected $defaultListParameters;

    /**
     * @var array
     */
    protected $listParameters;

    /**
     * @var string
     */
    protected $pagerAdapterClass;

    /**
     * Construct.
     *
     * @param FormFactory $formFactory
     * @param QueryBuilderUpdater $filterQueryBuilder
     */
    public function __construct(FormFactory $formFactory, FilterBuilderUpdaterInterface $filterQueryBuilder)
    {
        $this->formFactory           = $formFactory;
        $this->filterQueryBuilder    = $filterQueryBuilder;
        $this->storageNamespace      = 'filter_parameters';
        $this->defaultListParameters = array(
            'sort'         => array('id', 'asc'),
            'page'         => 1,
            'max_per_page' => 20,
            'filter_data'  => array(),
        );
        $this->listParameters        = array();
        $this->pagerAdapterClass     = 'Pagerfanta\Adapter\DoctrineORMAdapter';
    }

    /**
     * Set storageNamespace
     *
     * @param string $name
     * @return \Lexik\Bundle\CrudBundle\Filter\FilterManager
     */
    public function setStorageNamespace($name)
    {
        $this->storageNamespace = $name;

        return $this;
    }

    /**
     * Set the adapter class to use with the pager instance.
     *
     * @param string $class
     * @return \Lexik\Bundle\CrudBundle\Filter\FilterManager
     */
    public function setPagerAdapterClass($class)
    {
        if (!class_exists($class)) {
            throw new \RuntimeException(sprintf('The given class "%s" does not exist.'));
        }

        $this->pagerAdapterClass = $class;

        return $this;
    }

    /**
     * Set the http request request
     *
     * @param Request $request
     * @return \Lexik\Bundle\CrudBundle\Filter\FilterManager
     */
    public function setRequest(Request $request)
    {
        $this->request = $request;

        $this->restore(); // restore parameters
        $this->initializeParameters();

        return $this;
    }

    /**
     * Get the http request request
     *
     * @throws \RuntimeException
     * @return \Symfony\Component\HttpFoundation\Request
     */
    public function getRequest()
    {
        if (!($this->request instanceof Request)) {
            throw new \RuntimeException('The request must be a valid instance of Request');
        }

        return $this->request;
    }

    /**
     * Set queryBuilder
     *
     * @param \Doctrine\ORM\QueryBuilder $queryBuilder
     * @return FilterManager
     */
    public function setQueryBuilder($queryBuilder)
    {
        $this->queryBuilder = $queryBuilder;

        return $this;
    }

    /**
     * Get queryBuilder
     *
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function getQueryBuilder()
    {
        return $this->queryBuilder;
    }

    /**
     * Returns the query object from the query builder.
     *
     * @return \Doctrine\ORM\Query
     */
    public function getQuery()
    {
        return $this->queryBuilder->getQuery();
    }

    /**
     * Get formFilter
     *
     * @return \Symfony\Component\Form\Form
     */
    public function getFormFilter()
    {
        return $this->formFilter;
    }

    /**
     * Returns the form filter query builder updater instance.
     *
     * @return \Lexik\Bundle\FormFilterBundle\Filter\QueryBuilderUpdater
     */
    public function getQueryBuilderUpdater()
    {
        return $this->filterQueryBuilder;
    }

    /**
     * Initialize list parameters from the request.
     *
     * @return \Lexik\Bundle\CrudBundle\Filter\FilterManager
     */
    public function initializeParameters()
    {
        $store = false;

        if ($this->getRequest()->query->has('page')) {
            $this->setListParameter('page', $this->getRequest()->query->get('page'));

            $this->getRequest()->query->remove('page');

            $store = true;
        }

        if ($this->getRequest()->query->has('sort_by') || $this->getRequest()->query->has('sort_order')) {
            $this->setListParameter('sort', array(
                $this->getRequest()->query->get('sort_by'),
                $this->getRequest()->query->get('sort_order'),
            ));
            $this->setListParameter('page', 1);

            $this->getRequest()->query->remove('sort_by');
            $this->getRequest()->query->remove('sort_order');

            $store = true;
        }

        if ($this->getRequest()->query->has('maxPerPage')) {
            $this->setListParameter('max_per_page', $this->getRequest()->query->get('maxPerPage'));
            $this->setListParameter('page', 1);

            $this->getRequest()->query->remove('maxPerPage');

            $store = true;
        }

        if ($store) {
            $this->store();
        }

        return $this;
    }

    /**
     * Initialize the form filter.
     *
     * @param mixed $type    class namespace or form type name
     * @param array $options
     * @return \Lexik\Bundle\CrudBundle\Filter\FilterManager
     */
    public function initializeFormFilter($type, $options = array())
    {
        if (null != $type) {
            if (!is_object($type) && class_exists($type)) {
                $type = new $type();
            }

            $this->formFilter = $this->formFactory->create($type, null, $options);

            if ($this->getRequest()->query->has($this->formFilter->getName())) {
                $this->setListParameter('filter_data', $this->getRequest()->query->get($this->formFilter->getName()));
                $this->getRequest()->query->remove($this->formFilter->getName());
                $this->setListParameter('page', 1);

                $this->store();
            }
        }

        return $this;
    }

    /**
     * Add filter sort condition to the query builder.
     *
     * @return \Lexik\Bundle\CrudBundle\Filter\FilterManager
     */
    public function addSortConditions()
    {
        list($sortBy, $sortOrder) = $this->getListParameter('sort');

        if (null !== $sortBy) {
            if (strpos($sortBy, '.') === false) {
                // add the current alias
                $sortBy = sprintf('%s.%s', $this->queryBuilder->getRootAlias(), $sortBy);
            }
            $this->queryBuilder->orderBy($sortBy, $sortOrder);
        }

        return $this;
    }

    /**
     * Add form filter condition to the query builder.
     *
     * @return \Lexik\Bundle\CrudBundle\Filter\FilterManager
     */
    public function addFormFilterConditions()
    {
        if ($this->formFilter instanceof Form && count($this->getListParameter('filter_data'))) {

            if ( ! $this->formFilter->isSubmitted() ) {
                $this->formFilter->submit($this->getListParameter('filter_data'));
            }

            $this->filterQueryBuilder->addFilterConditions($this->formFilter, $this->queryBuilder);
        }

        return $this;
    }

    /**
     * Set a list parameter value.
     *
     * @param string $name
     * @param mixed $value
     * @return \Lexik\Bundle\CrudBundle\Filter\FilterManager
     */
    public function setListParameter($name, $value)
    {
        if (array_key_exists($name, $this->listParameters)) {
            $this->listParameters[$name] = $value;
        }

        return $this;
    }

    /**
     * Get a list parameter value.
     *
     * @param string $name
     * @return mixed
     */
    public function getListParameter($name)
    {
        return array_key_exists($name, $this->listParameters) ? $this->listParameters[$name] : false;
    }

    /**
     * Get all list parameters.
     *
     * @return array
     */
    public function getListParameters()
    {
        return $this->listParameters;
    }

    /**
     * Store list parameters in session.
     *
     * @return \Lexik\Bundle\CrudBundle\Filter\FilterManager
     */
    public function store()
    {
        $this->getRequest()->getSession()->set($this->storageNamespace, $this->serializeListParameters());

        return $this;
    }

    /**
     * Get list parameters from the session.
     *
     * @return \Lexik\Bundle\CrudBundle\Filter\FilterManager
     */
    public function restore()
    {
        $this->unserializeListParameters($this->getRequest()->getSession()->get($this->storageNamespace));

        $this->listParameters = array_merge($this->getDefaultListParameters(), $this->listParameters);

        return $this;
    }

    /**
     * Reset list parameters.
     *
     * @return \Lexik\Bundle\CrudBundle\Filter\FilterManager
     */
    public function reset()
    {
        $this->listParameters = $this->getDefaultListParameters();
        $this->store();

        return $this;
    }

    /**
     * Create a pagerfanta instance.
     *
     * @param string $type Pagination type
     * @param string $query In case you want to override default query (with some custom hints for example)
     *
     * @return \Pagerfanta\Pagerfanta
     */
    public function createPager($type = null, Query $query = null)
    {
        $pager = null;

        if (null === $type) {
            $type = self::PAGER_FANTA;
        }

        if (self::PAGER_FANTA === $type) {
            $adapterClass = $this->pagerAdapterClass;

            $adapter = new $adapterClass($query ?: $this->getQuery());
            $pager = new Pagerfanta($adapter);
            $pager->setMaxPerPage($this->getListParameter('max_per_page'));

            try {
                $pager->setCurrentPage($this->getListParameter('page'));
            } catch (NotValidCurrentPageException $e) {
                $pager->setCurrentPage(1);
        	}

        } else if (self::PAGER_SINGLE_PAGE === $type) {
            $pager = new SinglePagePager($this->getQuery());
        }

        return $pager;
    }

    /**
     * Get defauilt list parameters.
     *
     * @return array
     */
    public function getDefaultListParameters()
    {
        return $this->defaultListParameters;
    }

    /**
     * Set defauilt list parameters.
     *
     * @param array $parameters
     */
    public function setDefaultListParameters(array $parameters)
    {
        $this->defaultListParameters = array_merge($this->defaultListParameters, $parameters);
    }

    /**
     * Serialize list parameters.
     *
     * @return string
     */
    protected function serializeListParameters()
    {
        return serialize($this->listParameters);
    }

    /**
     * Unserialize list parameters.
     *
     * @param string $serialized
     */
    protected function unserializeListParameters($serialized)
    {
        $data = unserialize($serialized);
        if (is_array($data)) {
            $this->listParameters = $data;
        }
    }
}
