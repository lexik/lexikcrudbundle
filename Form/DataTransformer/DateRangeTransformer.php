<?php

namespace Lexik\Bundle\CrudBundle\Form\DataTransformer;

use Symfony\Component\Form\Extension\Core\DataTransformer\DateTimeToLocalizedStringTransformer;
use Symfony\Component\Form\Exception\UnexpectedTypeException;

/**
 * Date Range Transformer
 *
 * @package LexikCrudBundle
 * @author  Bérenger VIDAL <b.vidal@lexik.fr>
 */
class DateRangeTransformer extends DateTimeToLocalizedStringTransformer
{
    /**
     * Transforms a array of  normalized date into a array of  localized date string/array.
     *
     * @param array $dateTimes
     *
     * @return array
     */
    public function transform($dateTimes)
    {
        if (null === $dateTimes) {
            return;
        }

        if ( ! is_array($dateTimes)) {
            throw new UnexpectedTypeException($dateTimes, 'array');
        }

        $results = array();

        foreach ($dateTimes as $key => $dateTime) {
           if (null !== $dateTime) {
               $results[$key] = parent::transform($dateTime);
           }
        }

        return $results;
    }

    /**
     * Transforms a array of localized dates string/array into a array of normalized date.
     *
     * @param array $values
     *
     * @return array
     */
    public function reverseTransform($values)
    {
        if (null === $values) {
            return;
        }

        if ( ! is_array($values)) {
            throw new UnexpectedTypeException($values, 'array');
        }

        $results = array();

        foreach ($values as $key => $value) {
            if (null !== $value) {
                $results[$key] = parent::reverseTransform($value);
            }
        }

        return $results;
    }
}
