<?php

namespace Lexik\Bundle\CrudBundle\Form\DataTransformer;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\EntityManager;

use Lexik\Bundle\CrudBundle\Helper\EntityHelper;

use Symfony\Component\Form\DataTransformerInterface;

/**
 * Transform an Entity to ids ID.
 *
 * @author Cédric Girard <c.girard@lexik.fr>
 */
class EntityToIdentifierTransformer implements DataTransformerInterface
{
    /**
     * @var string
     */
    private $class;

    /**
     * @var EntityHelper
     */
    private $helper;

    /**
     * @var boolean
     */
    private $multiple;

    /**
     *
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em, $class, $multiple)
    {
        $this->helper = new EntityHelper($em, $class);
        $this->class = $class;
        $this->multiple = $multiple;
    }

    /**
     * {@inheritdoc}
     */
    public function transform($value)
    {
        if ($this->multiple) {
            if ( !(is_array($value) || $value instanceof Collection) ) {
                $value = $this->normalizeMultipleValue($value);
            }

            $ids = array();
            foreach ($value as $entity) {
                $ids[] = $this->helper->getEntityIdentifier($entity);
            }

            $value = $ids;

        } else if ($value instanceof $this->class) {
            $value = $this->helper->getEntityIdentifier($value);
        }

        return $value;
    }

    /**
     * {@inheritdoc}
     */
    public function reverseTransform($value)
    {
        if ($this->multiple) {
            $value = $this->normalizeMultipleValue($value);
            $value = new ArrayCollection($this->helper->getEntitiesByIds($value));

        } else if (!empty($value)) {
            $value = $this->helper->getEntityById($value);

        } else {
            $value = null;
        }

        return $value;
    }

    /**
     * Convert the given value to an array.
     *
     * @param mixed $value
     * @return array
     */
    private function normalizeMultipleValue($value)
    {
        if ('' === $value || null === $value) {
            $value = array();
        } else {
            $value = (array) $value;
        }

        return $value;
    }
}
