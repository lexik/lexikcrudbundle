<?php

namespace Lexik\Bundle\CrudBundle\Template;

use Lexik\Bundle\CrudBundle\Controller\CrudController;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Kernel;
use Symfony\Bundle\FrameworkBundle\Templating\TemplateReference;
use Symfony\Bundle\FrameworkBundle\Templating\Loader\TemplateLocator;

use Sensio\Bundle\FrameworkExtraBundle\EventListener\TemplateListener;

class TemplateSelector
{
    /**
     * @var array
     */
    private $fallbacks;

    /**
     * @var array
     */
    private $nameByActions;

    /**
     * @var Request
     */
    private $request;

    /**
     * @var CrudController
     */
    private $controller;

    /**
     * @var array
     */
    private $templates;

    /**
     * @var string
     */
    private $engine;

    /**
     * @var Kernel
     */
    private $kernel;

    /**
     * @var TemplateLocator
     */
    private $templateLocator;

    /**
     * Construct.
     *
     * @param array           $defaultTempaltes
     * @param array           $defaultTemplateNamesByActions
     * @param Kernel          $kernel
     * @param TemplateLocator $templateLocator
     */
    public function __construct($defaultTempaltes, $defaultTemplateNamesByActions, Kernel $kernel, TemplateLocator $templateLocator)
    {
        $this->fallbacks       = $defaultTempaltes;
        $this->nameByActions   = $defaultTemplateNamesByActions;
        $this->kernel          = $kernel;
        $this->templateLocator = $templateLocator;
        $this->engine          = 'twig';
    }

    /**
     * Set the twig engine name.
     *
     * @param string $engine
     */
    public function setEngine($engine)
    {
        $this->engine = $engine;
    }

    /**
     * Set the http request.
     *
     * @param Request $request
     */
    public function setRequest(Request $request)
    {
        $this->request = $request;
    }

    /**
     * Set the crud controller.
     *
     * @param CrudController $controller
     */
    public function setController(CrudController $controller)
    {
        $this->controller = $controller;
    }

    /**
     * Set an array of templates.
     *
     * @param array $templates
     */
    public function setTemplates(array $templates)
    {
        $this->templates = $templates;
    }

    /**
     * Set a specific template.
     *
     * @param string $name
     * @param string $template
     */
    public function setTemplate($name, $template)
    {
        $this->templates[$name] = $template;
    }

    /**
     * Returns the template name for the given action name.
     *
     * @param string $name
     *
     * @return string
     */
    public function getTemplate($name)
    {
        $template = null;

        if (!empty($this->templates[$name])) {
            return $this->templates[$name];
        } else {
            $templateReference = $this->guessTemplate($this->controller, $name, $this->request, $this->engine);

            // is there a different template name for this action?
            if (isset($this->nameByActions[$name])) {
                $templateReference->set('name', $this->nameByActions[$name]);
            }

            try {
                $this->templateLocator->locate($templateReference);
                return $templateReference->getLogicalName();
            } catch (\InvalidArgumentException $exp) {
                $template = null;
            }
        }

        return $this->getFallback($name);
    }

    /**
     * Returns the fallback template for the given action name.
     *
     * @param string $name
     *
     * @return string
     *
     * @throws \RuntimeException
     */
    protected function getFallback($name)
    {
        if (!isset($this->fallbacks[$name])) {
            throw new \RuntimeException(sprintf('No fallback template found for "%s"', $name));
        }

        return $this->fallbacks[$name];
    }

    /**
     * Guess the template name.
     *
     * @param CrudController $controller
     * @param string         $name
     * @param Request        $request
     * @param string         $engine
     *
     * @return Symfony\Bundle\FrameworkBundle\Templating\TemplateReference
     *
     * @throws \InvalidArgumentException
     */
    protected function guessTemplate(CrudController $controller, $name, Request $request, $engine)
    {
        $controllerClass = get_class($controller);

        if (!preg_match('/Controller\\\(.+)Controller$/', $controllerClass, $matchController)) {
            throw new \InvalidArgumentException(sprintf('The "%s" class is not a valid controller class.', $controllerClass));
        }

        $method = ucfirst($name);
        $reflectionClass = new \ReflectionClass($controllerClass);
        if (!$reflectionClass->hasMethod(sprintf('%sAction', $method))) {
            throw new \InvalidArgumentException(sprintf('The "%s" method is not a valid action.', $method));
        }

        $bundle = $this->getBundleForClass($controllerClass);

        return new TemplateReference($bundle->getName(), $matchController[1], $name, $request->getRequestFormat(), $engine);
    }

    /**
     * Returns the bundle anem of the given class.
     *
     * @param string $class
     *
     * @throws \InvalidArgumentException
     */
    protected function getBundleForClass($class)
    {
        $namespace = strtr(dirname(strtr($class, '\\', '/')), '/', '\\');

        foreach ($this->kernel->getBundles() as $bundle) {
            if (0 === strpos($namespace, $bundle->getNamespace())) {
                return $bundle;
            }
        }

        throw new \InvalidArgumentException(sprintf('The "%s" class does not belong to a registered bundle.', $class));
    }
}
