<?php

namespace Lexik\Bundle\CrudBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;
use Symfony\Component\DependencyInjection\ContainerBuilder;

use Lexik\Bundle\CrudBundle\DependencyInjection\Compiler\CrudControllersPass;

class LexikCrudBundle extends Bundle
{
    public function build(ContainerBuilder $container)
    {
        parent::build($container);

        $container->addCompilerPass(new CrudControllersPass());
    }
}
