<?php

namespace Lexik\Bundle\CrudBundle\Filter\Type;

use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\AbstractType;

use Lexik\Bundle\FormFilterBundle\Filter\ORM\Expr;

use Doctrine\ORM\QueryBuilder;

/**
 * AutocompleteChoiceType let you select an entity object through an autocomplete.
 *
 * @package LexikCrudBundle
 * @author  Jeremy Barthe <j.barthe@lexik.fr>
 */
class AutocompleteChoiceType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        parent::setDefaultOptions($resolver);

        $resolver
            ->setDefaults(array(
                'transformer_id' => 'lexik_form_filter.transformer.default',
             ))
            ->setAllowedValues(array(
                'transformer_id' => array('lexik_form_filter.transformer.default'),
            ))
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function getParent()
    {
        return 'lexik_autocomplete_choice';
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'lexik_filter_autocomplete_choice';
    }
}
