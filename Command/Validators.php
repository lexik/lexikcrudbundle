<?php
namespace Lexik\Bundle\CrudBundle\Command;

use Sensio\Bundle\GeneratorBundle\Command\Validators as BaseValidators;
use Doctrine\Common\Util\Inflector;

/**
 * Validators
 *
 * @author Laurent Heurtault <l.heurtault@lexikteam.com>
 */
class Validators extends BaseValidators
{
    /**
     * Validate and split an entity shortcut string
     *
     * @param string $shortcut
     *
     * @return array
     *
     * @throws \InvalidArgumentException
     */
    static public function parseShortcutNotation($shortcut)
    {
        if (false === $pos = strpos($shortcut, ':')) {
            throw new \InvalidArgumentException(sprintf('The entity name must contain a : ("%s" given, expecting something like AcmeBlogBundle:Blog/Post)', $shortcut));
        }

        return array(substr($shortcut, 0, $pos), substr($shortcut, $pos + 1));
    }

    /**
     * Get entity name
     *
     * @param string $shortcut
     *
     * @return string
     */
    static public function getEntityName($shortcut)
    {
        list($bundle, $entity) = self::parseShortcutNotation($shortcut);

        return $entity;
    }

    /**
     * Get bundle name
     *
     * @param string $shortcut
     *
     * @return string
     */
    static public function getBundleName($shortcut)
    {
        list($bundle, $entity) = self::parseShortcutNotation($shortcut);

        return $bundle;
    }

    /**
     * Generate crud service id:
     *      "lexik_user_bundle.crud.backend_user"
     *
     * @param string $shortcut
     *
     * @return string
     */
    static public function generateCrudServiceId($shortcut)
    {
        list($bundle, $entity) = self::parseShortcutNotation($shortcut);

        $serviceId = sprintf('%s.crud.%s',
            Inflector::tableize($bundle),
            Inflector::tableize(str_replace('/', '_', $entity))
        );

        return $serviceId;
    }

    /**
     * Generate the route name of a crud
     *
     * @param string $shortcut
     *
     * @return string
     */
    static public function generateCrudRouteName($shortcut)
    {
        list($bundle, $entity) = self::parseShortcutNotation($shortcut);

        $route = sprintf('crud_%s_%s',
            str_replace('_bundle', '', Inflector::tableize($bundle)),
            Inflector::tableize(str_replace('/', '_', $entity))
        );

        return $route;
    }

    /**
     * Generate the resource value of the routing entry of a crud
     *
     * @param string $shortcut
     *
     * @return string
     */
    static public function generateCrudRouteResource($shortcut)
    {
        return sprintf('@%s/Controller/%s',
            self::getBundleName($shortcut),
            self::generateCrudControllerFilename($shortcut)
        );
    }

    /**
     * Generate the relative filename of a crud controller
     *
     * @param string $shortcut
     *
     * @return string
     */
    static public function generateCrudControllerFilename($shortcut)
    {
        return sprintf('%sController.php', self::getEntityName($shortcut));
    }

    /**
     * Generate the filename of a crud form.
     *
     * @param string $shortcut
     *
     * @return string
     */
    static public function generateCrudFormFilename($shortcut)
    {
        return sprintf('%sType.php', self::getEntityName($shortcut));
    }
}
