<?php

namespace Lexik\Bundle\CrudBundle\Routing;

use Symfony\Component\Routing\Loader\AnnotationFileLoader;

/**
 * RoutingFileLoader prepare RoutingClassLoader to load CRUD routes.
 *
 * @package LexikCrudBundle
 * @author  Jeremy Barthe <j.barthe@lexik.fr>
 */
class RoutingFileLoader extends AnnotationFileLoader
{
    /**
     * {@inheritdoc}
     */
    public function supports($resource, $type = null)
    {
        return 'lexikcrud' == $type;
    }
}
