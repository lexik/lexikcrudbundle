<?php

namespace Lexik\Bundle\CrudBundle\Form\Extension;

use Lexik\Bundle\CrudBundle\Form\Type;

use Symfony\Component\Form\AbstractExtension;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Symfony\Bridge\Doctrine\Form\DoctrineOrmTypeGuesser;

/**
 * Represents the CRUD form extension (usefull for unit tests).
 *
 * @package LexikCrudBundle
 * @author  Jeremy Barthe <j.barthe@lexik.fr>
 */
class CrudExtension extends AbstractExtension
{
    /**
     * @var RegistryInterface
     */
    protected $registry;

    /**
     * Constructor.
     *
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry = null)
    {
        $this->registry = $registry;
    }

    /**
     * {@inheritdoc}
     */
    protected function loadTypes()
    {
        $defaultTypes = array(
            new Type\VatChoicerType(),
        );

        if (null !== $this->registry) {
            return array_merge($defaultTypes, array(
                new Type\AutocompleteChoiceType(),
                new Type\EntityChoicerType($this->registry),
                new Type\ModalboxChoiceType(),
            ));
        }

        return $defaultTypes;
    }
}
