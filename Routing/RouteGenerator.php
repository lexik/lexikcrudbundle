<?php

namespace Lexik\Bundle\CrudBundle\Routing;

use Symfony\Bundle\FrameworkBundle\Routing\Router;

class RouteGenerator
{
    /**
     * @var Symfony\Bundle\FrameworkBundle\Routing\Router
     */
    private $router;

    /**
     * @var string
     */
    private $baseRouteName;

    /**
     * @var array
     */
    private $additionalParameters;

    /**
     * Construct.
     *
     * @param Router $router
     */
    public function __construct(Router $router)
    {
        $this->router = $router;
        $this->additionalParameters = array();
    }

    /**
     * Set baseRouteName
     *
     * @param string $name
     */
    public function setBaseRouteName($name)
    {
        $this->baseRouteName = $name;
    }

    /**
     * Get baseRouteName
     *
     * @return string
     */
    public function getBaseRouteName()
    {
        return $this->baseRouteName;
    }

    /**
     * Set a given additional parameter
     *
     * @param string $parameter
     * @param mixed  $value
     */
    public function setAdditionalParameter($parameter, $value)
    {
        $this->additionalParameters[$parameter] = $value;
    }

    /**
     * Set additionalParameters
     *
     * @param array $parameters
     */
    public function setAdditionalParameters($parameters)
    {
        $this->additionalParameters = $parameters;
    }

    /**
     * Get additionalParameters
     *
     * @return array
     */
    public function getAdditionalParameters()
    {
        return $this->additionalParameters;
    }

    /**
     * Returns the crud route name for the given action
     *
     * @param string $name
     * @return string
     */
    public function getRouteName($name)
    {
        return sprintf('%s_%s', $this->baseRouteName, $name);
    }

    /**
     * Generate an url for the given crud action name.
     *
     * @param string $name
     * @param array $parameters
     * @param boolean $absolute
     * @return string
     */
    public function generateUrl($name, $parameters = array(), $absolute = false)
    {
        $parameters = array_merge($this->additionalParameters, $parameters);

        return $this->router->generate($this->getRouteName($name), $parameters, $absolute);
    }
}