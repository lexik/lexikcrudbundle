<?php

namespace Lexik\Bundle\CrudBundle\Twig\Extension;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException;

use Lexik\Bundle\CrudBundle\Helper\EntityHelper;

/**
 * CRUD extension: some usefuls functions for twig templates.
 *
 * @package LexikCrudBundle
 * @author  Jeremy Barthe <j.barthe@lexik.fr>
 */
class CrudExtension extends \Twig_Extension
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * Constructor.
     *
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * {@inheritdoc}
     */
    public function getFunctions()
    {
        return array(
            'crud_path'           => new \Twig_Function_Method($this, 'getPath'),
            'crud_url'            => new \Twig_Function_Method($this, 'getUrl'),
            'crud_object_path'    => new \Twig_Function_Method($this, 'getObjectPath'),
            'crud_object_url'     => new \Twig_Function_Method($this, 'getObjectUrl'),
            'crud_sort_path'      => new \Twig_Function_Method($this, 'getSortPath'),
            'crud_sort_url'       => new \Twig_Function_Method($this, 'getSortUrl'),
            'crud_get_sort_order' => new \Twig_Function_Method($this, 'getSortOrderByColumn'),
            'crud_is_granted'     => new \Twig_Function_Method($this, 'isGranted'),
            'crud_list_param'     => new \Twig_Function_Method($this, 'getListParameter'),
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getGlobals()
    {
        return array(
            'base_template' => $this->container->getParameter('lexik_crud.base_template'),
        );
    }

    /**
     * Returns the name of the extension.
     *
     * @return string The extension name
     */
    public function getName()
    {
        return 'lexik_crud';
    }

    /**
     * Return a CRUD controller service.
     *
     * @param string $crud
     *
     * @return \Lexik\Bundle\CrudBundle\Controller\CrudController
     */
    public function getCrud($crud)
    {
        return $this->container->get($crud);
    }

    /**
     * Return an URL for a given CRUD and a given action.
     *
     * @param string $crud
     * @param string $name
     * @param array  $parameters
     * @param bool   $absolute
     *
     * @return string
     */
    protected function generateCrudUrl($crud, $name, $parameters = array(), $absolute = false)
    {
        try {
            return $this->getCrud($crud)->getRouteGenerator()->generateUrl($name, $parameters, $absolute);
        } catch (ServiceNotFoundException $e) {
            return '#';
        }
    }

    /**
     * Return a relative URL for a given CRUD and a given action.
     *
     * @param string $crud
     * @param string $name
     * @param array  $parameters
     *
     * @return string
     */
    public function getPath($crud, $name, $parameters = array())
    {
        return $this->generateCrudUrl($crud, $name, $parameters, false);
    }

    /**
     * Return an absolute URL for a given CRUD and a given action.
     *
     * @param string $crud
     * @param string $name
     * @param array  $parameters
     *
     * @return string
     */
    public function getUrl($crud, $name, $parameters = array())
    {
        return $this->generateCrudUrl($crud, $name, $parameters, true);
    }

    /**
     * Return an URL for edit or show an object, accorging to user roles, of a given CRUD.
     *
     * @param string $crud
     * @param object $object
     * @param array  $parameters
     * @param bool   $absolute
     *
     * @return string
     */
    protected function generateObjectCrudUrl($crud, $object, $parameters = array(), $absolute = false)
    {
        try {
            $controller = $this->getCrud($crud);

            $entityHelper = new EntityHelper($this->container->get('doctrine.orm.entity_manager'), get_class($object));

            $parameters = array_merge($entityHelper->getIdentifierValues($object), $parameters);

            if ($controller->isGranted('edit', $object)) {
                return $controller->getRouteGenerator()->generateUrl('edit', $parameters, $absolute);
            }

            if ($controller->isGranted('show', $object)) {
                return $controller->getRouteGenerator()->generateUrl('show', $parameters, $absolute);
            }

            return '#';
        } catch (\Exception $e) {
            return '#';
        }
    }

    /**
     * Return a relative URL for edit or show an object, accorging to user roles, of a given CRUD.
     *
     * @param string $crud
     * @param object $object
     * @param array  $parameters
     *
     * @return string
     */
    public function getObjectPath($crud, $object, $parameters = array())
    {
        return $this->generateObjectCrudUrl($crud, $object, $parameters, false);
    }

    /**
     * Return an absolute URL for edit or show an object, accorging to user roles, of a given CRUD.
     *
     * @param string $crud
     * @param object $object
     * @param array  $parameters
     *
     * @return string
     */
    public function getObjectUrl($crud, $object, $parameters = array())
    {
        return $this->generateObjectCrudUrl($crud, $object, $parameters, true);
    }

    /**
     * Return sort order for current sort column.
     *
     * @param string $crud
     * @param string $column
     *
     * @return null|string
     */
    public function getSortOrderByColumn($crud, $column)
    {
        list($sortBy, $sortOrder) = $this->getListParameter($crud, 'sort');

        if ($sortBy == $column) {
            return $sortOrder;
        }
    }

    /**
     * Return an URL to sort a crud on a given column.
     *
     * @param string $crud
     * @param string $column
     * @param array  $parameters
     * @param bool   $absolute
     *
     * @return string
     */
    protected function generateSortUrl($crud, $column, $parameters = array(), $absolute = false)
    {
        try {
            $route = 'list';
            if (!empty($parameters['route'])) {
                $route = $parameters['route'];
                unset($parameters['route']);
            }

            $parameters = array_merge(
                $this->container->get('request')->query->all(),
                $parameters,
                array(
                    'sort_by'    => $column,
                    'sort_order' => ($this->getSortOrderByColumn($crud, $column) == 'asc') ? 'desc' : 'asc',
                )
            );

            if ('list' == $route) {
                $url = $this->getCrud($crud)->getRouteGenerator()->generateUrl('list', $parameters, $absolute);
            } else {
                $url = $this->getCrud($crud)->get('router')->generate($route, $parameters, $absolute);
            }

            return $url;
        } catch (ServiceNotFoundException $e) {
            return '#';
        }
    }

    /**
     * Return a relative URL to sort a crud on a given column.
     *
     * @param string $crud
     * @param string $column
     * @param array  $parameters
     *
     * @return string
     */
    public function getSortPath($crud, $column, $parameters = array())
    {
        return $this->generateSortUrl($crud, $column, $parameters, false);
    }

    /**
     * Return an absolute URL to sort a crud on a given column.
     *
     * @param string $crud
     * @param string $column
     * @param array  $parameters
     *
     * @return string
     */
    public function getSortUrl($crud, $column, $parameters = array())
    {
        return $this->generateSortUrl($crud, $column, $parameters, true);
    }

    /**
     * Checks if the requested action is granted against the user roles.
     *
     * @param string      $crud
     * @param string      $action
     * @param null|object $object
     *
     * @return bool
     */
    public function isGranted($crud, $action, $object = null)
    {
        try {
            return $this->getCrud($crud)->isGranted($action, $object);
        } catch (ServiceNotFoundException $e) {
            return false;
        }
    }

    /**
     * Return a given list parameter.
     *
     * @param string $crud
     * @param string $name
     *
     * @return mixed
     */
    public function getListParameter($crud, $name)
    {
        try {
            return $this->getCrud($crud)->getFilterManager()->getListParameter($name);
        } catch (ServiceNotFoundException $e) {
            return '';
        }
    }
}
