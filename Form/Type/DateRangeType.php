<?php

namespace Lexik\Bundle\CrudBundle\Form\Type;

use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\Extension\Core\DataTransformer\DateTimeToArrayTransformer;

use Lexik\Bundle\CrudBundle\Form\DataTransformer\DateRangeTransformer;

/**
 * Date Range form type
 *
 * @package LexikCrudBundle
 * @author  Bérenger VIDAL <b.vidal@lexik.fr>
 */
class DateRangeType extends AbstractType
{
     /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('from', 'date', $options)
            ->add('to', 'date', $options)
        ;

        $builder
            ->addViewTransformer(new DateRangeTransformer(
                $options['data_timezone'],
                $options['user_timezone'],
                $options['format'],
                \IntlDateFormatter::NONE,
                \IntlDateFormatter::GREGORIAN,
                null
            ));
    }

    /**
     * {@inheritdoc}
     */
    public function getParent()
    {
        return 'form';
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        parent::setDefaultOptions($resolver);

        $resolver->setDefaults(array(
            'input'          => 'datetime',
            'data_timezone'  => null,
            'user_timezone'  => null,
            'widget'         => 'single_text',
            'format'         =>  \IntlDateFormatter::SHORT,
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'lexik_date_range';
    }
}
