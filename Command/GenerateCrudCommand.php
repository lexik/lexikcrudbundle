<?php

namespace Lexik\Bundle\CrudBundle\Command;

use Doctrine\Common\Persistence\Mapping\ClassMetadata;

use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Output\Output;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;

use Lexik\Bundle\CrudBundle\Generator\CrudGenerator;
use Lexik\Bundle\CrudBundle\Manipulator\ServiceManipulator;
use Lexik\Bundle\CrudBundle\Manipulator\RoutingManipulator;
use Lexik\Bundle\CrudBundle\Command\Validators;

/**
 * GenerateCrudCommand class.
 *
 * @author Laurent Heurtault <l.heurtault@lexik.fr>
 * @author Olivier Maisonneuve <o.maisonneuve@lexik.fr>
 */
class GenerateCrudCommand extends ContainerAwareCommand
{
    /**
     * @var ClassMetadata
     */
    protected $metaData;

    /**
     * @see Command
     */
    protected function configure()
    {
        $this
            ->addArgument('entity', InputArgument::REQUIRED, 'The entity.')
            ->setDescription('Generates CRUD files.')
            ->setHelp(<<<EOS
Generates CRUD files.

    app/console lexik:crud:generate <AcmeFooBundle:Bar>

EOS
            )
            ->setName('lexik:crud:generate')
        ;
    }

    /**
     * @see Command
     *
     * @throws \InvalidArgumentException When namespace doesn't end with Bundle
     * @throws \RuntimeException         When bundle can't be executed
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $entityShortcut = Validators::validateEntityName($input->getArgument('entity'));

        $output->writeln(sprintf('Generating the LexikCrud for entity : <info>%s</info>', $entityShortcut));

        $entity = Validators::getEntityName($entityShortcut);

        $bundleNamespace = $this->getBundleNamespace($entityShortcut);
        $bundlePath = $this->getBundlePath($entityShortcut);

        // Generate Controller, Type and view files
        $generator = $this->getCrudGenerator();
        $generator->generate(
            $entityShortcut,
            $bundlePath,
            $bundleNamespace,
            $this->getEntityMetadata($entityShortcut)
        );

        // Update the services yml file
        $serviceManipulator = new ServiceManipulator($bundlePath.'Resources/config/services.yml');
        $serviceManipulator->addService(
            Validators::generateCrudServiceId($entityShortcut),
            $this->getEntityNamespace($entityShortcut),
            sprintf('%s\\Controller\\%sController', $bundleNamespace, str_replace('/', '\\', $entity))
        );

        // Update the routing yml file
        $routingManipulator = new RoutingManipulator($bundlePath.'Resources/config/routing.yml');
        $routingManipulator->addRoute(
            Validators::generateCrudRouteName($entityShortcut),
            Validators::generateCrudRouteResource($entityShortcut)
        );
    }

    /**
     * Returns entity's metadata.
     *
     * @param string $entityShortcut
     *
     * @return ClassMetadata
     */
    protected function getEntityMetadata($entityShortcut)
    {
        if (null === $this->metaData) {
            $em = $this->getContainer()->get('doctrine.orm.entity_manager');
            $this->metaData = $em->getClassMetadata($entityShortcut);
        }

        return $this->metaData;
    }

    /**
     * Return bundle's namespace.
     *
     * @param string $entityShortcut
     *
     * @return string
     */
    protected function getBundleNamespace($entityShortcut)
    {
        return str_replace(
            '\\Entity',
            '',
            $this->getEntityMetadata($entityShortcut)->namespace
        );
    }

    /**
     * Returns entity's namespace.
     *
     * @param string $entityShortcut
     *
     * @return string
     */
    protected function getEntityNamespace($entityShortcut)
    {
        return $this->getEntityMetadata($entityShortcut)->name;
    }

    /**
     * Returns bundle's root path.
     *
     * @param string  $entityShortcut
     *
     * @return string
     *
     * @throws \RuntimeException
     */
    protected function getBundlePath($entityShortcut)
    {
        $dir = sprintf(
            '%s/src/%s/',
            getcwd(),
            str_replace('\\', '/', $this->getBundleNamespace($entityShortcut))
        );

        if (!is_dir($dir)) {
            throw new \RuntimeException(sprintf('The "%s" path is not a valid directory.', $dir));
        }

        return $dir;
    }

    /**
     * Return CrudGenerator instance.
     *
     * @return CrudGenerator
     */
    protected function getCrudGenerator()
    {
        return new CrudGenerator(
            $this->getContainer()->get('filesystem'),
            __DIR__.'/../Resources/skeleton/bundle'
        );
    }
}
