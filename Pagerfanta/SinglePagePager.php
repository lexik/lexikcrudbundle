<?php

namespace Lexik\Bundle\CrudBundle\Pagerfanta;

use Doctrine\ORM\Query;

use Pagerfanta\PagerfantaInterface;

class SinglePagePager implements PagerfantaInterface
{
    /**
     * @var int
     */
    private $maxPerPage;

    /**
     * @var int
     */
    private $currentPage;

    /**
     * @var int
     */
    private $nbResults;

    /**
     * @var array
     */
    private $currentPageResults;

    /**
     * @var int
     */
    private $nbPages;

    /**
     * @var Query
     */
    private $query;

    /**
     *
     * @param Query $query
     */
    public function __construct(Query $query)
    {
        $this->maxPerPage  = 0;
        $this->currentPage = 1;
        $this->nbPages     = 1;
        $this->query       = $query;
    }

    /**
     * {@inheritdoc}
     */
    public function setMaxPerPage($maxPerPage)
    {
        return $this;
    }


    /**
     * {@inheritdoc}
     */
    public function getMaxPerPage()
    {
        return $this->maxPerPage;
    }


    /**
     * {@inheritdoc}
     */
    public function setCurrentPage($currentPage)
    {
        return $this;
    }


    /**
     * {@inheritdoc}
     */
    public function getCurrentPage()
    {
        return $this->currentPage;
    }


    /**
     * {@inheritdoc}
     */
    public function getCurrentPageResults()
    {
        if (null == $this->currentPageResults) {
            $this->currentPageResults = $this->query->getResult();
        }

        return $this->currentPageResults;
    }

    /**
     * {@inheritdoc}
     */
    public function getNbResults()
    {
        $this->nbResults = count($this->getCurrentPageResults());

        return $this->nbResults;
    }

    /**
     * {@inheritdoc}
     */
    public function getNbPages()
    {
        return $this->nbPages;
    }

    /**
     * {@inheritdoc}
     */
    public function haveToPaginate()
    {
        return false;
    }

    /**
     * {@inheritdoc}
     */
    public function hasPreviousPage()
    {
        return false;
    }

    /**
     * {@inheritdoc}
     */
    public function getPreviousPage()
    {
        throw new \RuntimeException('SinglePage pager does not have any previous page.');
    }

    /**
     * {@inheritdoc}
     */
    public function hasNextPage()
    {
        return false;
    }

    /**
     * {@inheritdoc}
     */
    public function getNextPage()
    {
        throw new \RuntimeException('SinglePage pager does not have any next page.');
    }

    /**
     * {@inheritdoc}
     */
    public function count()
    {
        return $this->getNbResults();
    }

    /**
     * {@inheritdoc}
     */
    public function getIterator()
    {
        return new \ArrayIterator($this->getCurrentPageResults());
    }
}