<?php

namespace Lexik\Bundle\CrudBundle\Form\Type;

use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\Form\FormInterface;
use Symfony\Bridge\Doctrine\Form\DataTransformer\EntityToIdTransformer;

/**
 * AutocompleteChoiceType let you select an entity object through an autocomplete.
 *
 * @package LexikCrudBundle
 * @author  Jeremy Barthe <j.barthe@lexik.fr>
 */
class AutocompleteChoiceType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->setAttribute('route_name', $options['route_name']);
        $builder->setAttribute('route_params', $options['route_params']);
        $builder->setAttribute('data_attr', $options['data_attr']);
    }

    /**
     * {@inheritdoc}
     */
    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        $view->vars['route_name'] = $form->getConfig()->getAttribute('route_name');
        $view->vars['route_params'] = $form->getConfig()->getAttribute('route_params');
        $view->vars['data_attr'] = $form->getConfig()->getAttribute('data_attr');
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        parent::setDefaultOptions($resolver);

        $resolver->setDefaults(array(
            'route_name'   => null,
            'route_params' => array(),
            'data_attr'    => array(),
        ));
    }


    /**
     * {@inheritdoc}
     */
    public function getParent()
    {
        return 'lexik_entity_choicer';
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'lexik_autocomplete_choice';
    }
}
