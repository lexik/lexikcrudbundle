<?php
namespace Lexik\Bundle\CrudBundle\Manipulator;

/**
 * Add the YAML definitions of a crud service to YAML config file
 *
 * @author Laurent Heurtault <l.heurtault@lexikteam.com>
 */
class ServiceManipulator
{
    private $file;

    /**
     * Constructor.
     *
     * @param string $file The YAML services config file path
     */
    public function __construct($file)
    {
        $this->file = $file;
    }

     /**
     * Adds a service resource a service config file
     *
     * @param string $entity full namespace of the entity
     * @param string $controller namespace of the created controller
     *
     * @return Boolean true if it worked, false otherwise
     *
     * @throws \RuntimeException If bundle is already imported
     */
    public function addService($serviceId, $entityClass, $controllerClass)
    {
        if (!file_exists($this->file)) {
            throw new \RuntimeException(sprintf('Service configuration file "%s" not found.', $this->file));
        }

        $current = file_get_contents($this->file);

        // Don't add same service twice
        if (false !== strpos($current, $serviceId)) {
            throw new \RuntimeException(sprintf('Service "%s" already declared in "%s".', $serviceId, $this->file));
        }

        $definition = <<<EOF
    $serviceId:
        class: '$controllerClass'
        arguments:
            - '@service_container'
            - '$entityClass'
        tags:
            - { name: lexik_crud.controller }

EOF;

        // locate the "services" section and add the definition there
        $code = '';
        if (false !== strpos($current, 'services:')) {
            foreach (explode("\n", $current) as $line) {
                $code .= $line."\n";
                if (false !== strpos($line, 'services:')) {
                    $code .= $definition;
                }
            }
        } else {
            $code = $current."\nservices:\n".$definition;
        }

        if (false === file_put_contents($this->file, $code)) {
            return false;
        }

        return true;
    }
}
